
using System;
using System.IO;
using System.IO.IsolatedStorage;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;


namespace BasicEffectSample
{
    /// <summary>

    public class Game1 : Microsoft.Xna.Framework.Game
    {
        SoundEffect eatHeart, soundOFcarAccident, hooray, beep_crash,beep_eat,beep_500;
        Song carsound;
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Model model0, model1, model2, modelheart;
        Texture2D fall, wanet1_left, wanet1_grade, wanet1_right, wanet2_left, wanet2_grade, wanet2_right, wanet3_left, wanet3_grade, soundoff, soundon, newgame, winner,levels_lock,
            summer, winter, wanet3_right, grade, left, right, lambur, hilux, red1, red2, red3, red4, red5, PausePic, PlayPic, help, ok, fullversion,back,stopmusic
            , red_1, red_2, red_3, red_4, red_5, green1, green2, green3, green4, green5, green_1, green_2, green_3, green_4, green_5, levels, continueSign, gameOver,jump,circle,SCORE;
        Matrix view, viewH;
        Matrix view3;
        int page, frame_counter, frame_counter2, heart_counter;
        Matrix proj;
        Matrix proj3;
        int Speed;
        Int64 RECORD, MAX, maxFORwin;
        Matrix word1;
        Vector3 pos0, pos1, pos2, pixelPosition0, pixelPositionH;
        SpriteFont font; SpriteFont angleFont, angleFont2;
        Vector3 cam_position;
        Vector3 modelPosition0, modelPosition1, modelPosition2, modelPositionH, modelPosition3;
        float modelRotation0, modelRotation1, modelRotation2, modelRotationH;
        int[,] CarsArrayPosition = new int[4, 2];
        int[] RoadPointAmount = new int[5];
        int[,] Veer_grade = new int[7, 20];
        int[,] Veer_divert = new int[7, 21];
        int[] CarsMode = new int[3];
        int[] autoVeergrade = new int[17];
        int[] autoVeerdivert = new int[17];
        int[] CarsRoute = new int[3];
        int[] parking = new int[3];
        int[,] STEPS = new int[2,3];
        Boolean[] Divert = new Boolean[3];
        Boolean[] leftORright = new Boolean[3];
        Boolean[] SW = new Boolean[3];
        Boolean accident, heartAccident, finish, SOUND, sw_firstStart, sw_acc;
        float[,] R1 = new float[3, 2500];
        float[,] FirstRotation = new float[4, 2];
        float[,] hitHeart = new float[2, 10];
        Boolean ampel1, ampel2, PAUSE, ampleSound, Unfinished, WIN, backORexit, cleanFile, oldpause, SW_stopBG,stepsSW,sw_a,sw_b,sw_c;
        int Level, RANDOM, upDown,stepsCounter,rd;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            //graphics.PreferredBackBufferHeight = 760; graphics.PreferredBackBufferWidth = 1280;
            //graphics.PreferredBackBufferHeight = 480; graphics.PreferredBackBufferWidth = 800;
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            //Guide.SimulateTrialMode = true;
            STEPS[0,0]=STEPS[0,1]=STEPS[0,2]=-1;
            STEPS[1, 0] = STEPS[1, 1] = STEPS[1, 2] = -1;
            stepsCounter = 0;
            upDown = 4;
            oldpause =sw_a=sw_b=sw_c= false;
            cleanFile =SW_stopBG= false;
            maxFORwin = 200000;
            if (Guide.IsTrialMode)
                maxFORwin = 2000;
            leftORright[0] = leftORright[1] = leftORright[2] = true;
            WIN = false;
            backORexit = false;
            RANDOM = 0;
            sw_acc = false;
            ampleSound = false;
            Unfinished = false;
            sw_firstStart = true;
            PAUSE = false;
            MAX = 0;
            finish = false;
            if (MediaPlayer.GameHasControl)
                SOUND = true;
            else SOUND = false;
            
            SW[0] = false; SW[1] = false; SW[2] = false;
            heartAccident = false;
            Divert[0] = false; Divert[1] = false; Divert[2] = false;
            Level = 1;
            Speed = 2;
            page = 1;
            frame_counter = 600; frame_counter2 = 300;
            heart_counter = 340;
            RECORD = 0;
            ampel1 = false; ampel2 = false;
            CarsArrayPosition[0, 0] = 1315; CarsArrayPosition[0, 1] = 1316;
            CarsArrayPosition[1, 0] = 1030; CarsArrayPosition[1, 1] = 1031;
            CarsArrayPosition[2, 0] = 2391; CarsArrayPosition[2, 1] = 2390;
            parking[0] = parking[1] = parking[2] = 0;
            accident = false;
            hitHeart[0, 0] = 100; hitHeart[1, 0] = 0;
            hitHeart[0, 1] = -250; hitHeart[1, 1] =-200 ;
            hitHeart[0, 2] = 0; hitHeart[1, 2] = 200;
            hitHeart[0, 3] = -450; hitHeart[1, 3] = 90;
            hitHeart[0, 4] = 180; hitHeart[1, 4] = 40;
            hitHeart[0, 5] = 0; hitHeart[1, 5] = -385;
            hitHeart[0, 6] = 90; hitHeart[1, 6] = -280;
            hitHeart[0, 7] = 390; hitHeart[1, 7] = -280;
            hitHeart[0, 8] = 90; hitHeart[1, 8] = -175;
            hitHeart[0, 9] = 250; hitHeart[1, 9] = 210;
            autoVeergrade[0] = 10; autoVeergrade[1] = 163; autoVeergrade[2] = 500; autoVeergrade[3] = 600; autoVeergrade[4] = 740; autoVeergrade[5] = 910; autoVeergrade[6] = 950;
            autoVeergrade[7] = 1130; autoVeergrade[8] = 1210; autoVeergrade[9] = 1250; autoVeergrade[10] = 1300; autoVeergrade[11] = 1600; autoVeergrade[12] = 1625; autoVeergrade[13] = 1686;
            autoVeergrade[14] = 1710; autoVeergrade[15] = 1785; autoVeergrade[16] = 1980;


            autoVeerdivert[0] = 2198; autoVeerdivert[1] = 1975; autoVeerdivert[2] = 1725; autoVeerdivert[3] = 1700; autoVeerdivert[4] = 1655; autoVeerdivert[5] = 1619; autoVeerdivert[6] = 1483;
            autoVeerdivert[7] = 1280; autoVeerdivert[8] = -1245; autoVeerdivert[9] = 1145; autoVeerdivert[10] = 1060; autoVeerdivert[11] = 930; autoVeerdivert[12] = 880; autoVeerdivert[13] = 720;
            autoVeerdivert[14] = 650; autoVeerdivert[15] = 390; autoVeerdivert[16] = 95;

            Veer_grade[0, 0] = 84; Veer_grade[1, 0] = 1210; Veer_grade[2, 0] = 1211; Veer_grade[3, 0] = 85; Veer_grade[4, 0] = 86; Veer_grade[5, 0] = 1165; Veer_grade[6, 0] = 1164;
            Veer_grade[0, 1] = 395; Veer_grade[1, 1] = -10; Veer_grade[2, 1] = -10; Veer_grade[3, 1] = 396; Veer_grade[4, 1] = 397; Veer_grade[5, 1] = 1280; Veer_grade[6, 1] = 1279;
            Veer_grade[0, 2] = 540; Veer_grade[1, 2] = 1285; Veer_grade[2, 2] = 1286; Veer_grade[3, 2] = 541; Veer_grade[4, 2] = 542; Veer_grade[5, 2] = -10; Veer_grade[6, 2] = -10;
            Veer_grade[0, 3] = 660; Veer_grade[1, 3] = 0; Veer_grade[2, 3] = 1; Veer_grade[3, 3] = 661; Veer_grade[4, 3] = 662; Veer_grade[5, 3] = -10; Veer_grade[6, 3] = -10;
            Veer_grade[0, 4] = 715; Veer_grade[1, 4] = 1623; Veer_grade[2, 4] = 1624; Veer_grade[3, 4] = 716; Veer_grade[4, 4] = 717; Veer_grade[5, 4] = -10; Veer_grade[6, 4] = -10;
            Veer_grade[0, 5] = 880; Veer_grade[1, 5] = -10; Veer_grade[2, 5] = -10; Veer_grade[3, 5] = 881; Veer_grade[4, 5] = 882; Veer_grade[5, 5] = 2203; Veer_grade[6, 5] = 2204;
            Veer_grade[0, 6] = 900; Veer_grade[1, 6] = 1619; Veer_grade[2, 6] = 1618; Veer_grade[3, 6] = 901; Veer_grade[4, 6] = 902; Veer_grade[5, 6] = -10; Veer_grade[6, 6] = -10;
            Veer_grade[0, 7] = 1073; Veer_grade[1, 7] = 1760; Veer_grade[2, 7] = 1761; Veer_grade[3, 7] = 1074; Veer_grade[4, 7] = 1075; Veer_grade[5, 7] = 1725; Veer_grade[6, 7] = 1724;
            Veer_grade[0, 8] = 1165; Veer_grade[1, 8] = 153; Veer_grade[2, 8] = 154; Veer_grade[3, 8] = 1166; Veer_grade[4, 8] = 1167; Veer_grade[5, 8] = 85; Veer_grade[6, 8] = 84;
            Veer_grade[0, 9] = 1235; Veer_grade[1, 9] = 1975; Veer_grade[2, 9] = 1974; Veer_grade[3, 9] = 1236; Veer_grade[4, 9] = 1237; Veer_grade[5, 9] = -10; Veer_grade[6, 9] = -10;
            Veer_grade[0, 10] = -1260; Veer_grade[1, 10] = -10; Veer_grade[2, 10] = -10; Veer_grade[3, 10] = 1261; Veer_grade[4, 10] = 1262; Veer_grade[5, 10] = 1980; Veer_grade[6, 10] = 1981;
            Veer_grade[0, 11] = 1280; Veer_grade[1, 11] = 395; Veer_grade[2, 11] = 394; Veer_grade[3, 11] = 445; Veer_grade[4, 11] = 446; Veer_grade[5, 11] = 445; Veer_grade[6, 11] = 446;
            Veer_grade[0, 12] = 1510; Veer_grade[1, 12] = 1693; Veer_grade[2, 12] = 1694; Veer_grade[3, 12] = 1511; Veer_grade[4, 12] = 1512; Veer_grade[5, 12] = 1645; Veer_grade[6, 12] = 1644;
            Veer_grade[0, 13] = 1619; Veer_grade[1, 13] = 920; Veer_grade[2, 13] = 921; Veer_grade[3, 13] = -10; Veer_grade[4, 13] = -10; Veer_grade[5, 13] = 895; Veer_grade[6, 13] = 894;
            Veer_grade[0, 14] = 1645; Veer_grade[1, 14] = 1510; Veer_grade[2, 14] = 1509; Veer_grade[3, 14] = 1646; Veer_grade[4, 14] = 1647; Veer_grade[5, 14] = 1578; Veer_grade[6, 14] = 1579;
            Veer_grade[0, 16] = 1725; Veer_grade[1, 16] = 1120; Veer_grade[2, 16] = 1121; Veer_grade[3, 16] = 1726; Veer_grade[4, 16] = 1727; Veer_grade[5, 16] = 1070; Veer_grade[6, 16] = 1069;
            Veer_grade[0, 17] = 1975; Veer_grade[1, 17] = 1245; Veer_grade[2, 17] = 1246; Veer_grade[3, 17] = 1245; Veer_grade[4, 17] = 1246; Veer_grade[5, 17] = 1240; Veer_grade[6, 17] = 1239;
            Veer_grade[0, 18] = 2198; Veer_grade[1, 18] = 1701; Veer_grade[2, 18] = 1702; Veer_grade[3, 18] = -10; Veer_grade[4, 18] = -10; Veer_grade[5, 18] = 1700; Veer_grade[6, 18] = 1699;
            Veer_grade[0, 15] = 2394; Veer_grade[1, 15] = -10; Veer_grade[2, 15] = -10; Veer_grade[3, 15] = 940; Veer_grade[4, 15] = 941; Veer_grade[5, 15] = -10; Veer_grade[6, 15] = -10;


            Veer_divert[0, 0] = 2204; Veer_divert[1, 0] = -10; Veer_divert[2, 0] = -10; Veer_divert[3, 0] = 880; Veer_divert[4, 0] = 879; Veer_divert[5, 0] = -10; Veer_divert[6, 0] = -10;
            Veer_divert[0, 1] = 730; Veer_divert[1, 1] = 1623; Veer_divert[2, 1] = 1624; Veer_divert[3, 1] = 729; Veer_divert[4, 1] = 728; Veer_divert[5, 1] = -10; Veer_divert[6, 1] = -10;
            Veer_divert[0, 2] = 700; Veer_divert[1, 2] = 0; Veer_divert[2, 2] = 1; Veer_divert[3, 2] = 698; Veer_divert[4, 2] = 697; Veer_divert[5, 2] = -10; Veer_divert[6, 2] = -10;
            Veer_divert[0, 3] = 570; Veer_divert[1, 3] = 1300; Veer_divert[2, 3] = 1301; Veer_divert[3, 3] = 569; Veer_divert[4, 3] = 568; Veer_divert[5, 3] = -10; Veer_divert[6, 3] = -10;
            Veer_divert[0, 4] = 450; Veer_divert[1, 4] = -10; Veer_divert[2, 4] = -10; Veer_divert[3, 4] = 449; Veer_divert[4, 4] = 448; Veer_divert[5, 4] = 1279; Veer_divert[6, 4] = 1278;
            Veer_divert[0, 5] = 153; Veer_divert[1, 5] = 1210; Veer_divert[2, 5] = 1211; Veer_divert[3, 5] = 152; Veer_divert[4, 5] = 151; Veer_divert[5, 5] = 1165; Veer_divert[6, 5] = 1164;
            Veer_divert[0, 6] = 1; Veer_divert[1, 6] = 685; Veer_divert[2, 6] = 684; Veer_divert[3, 6] = 685; Veer_divert[4, 6] = 686; Veer_divert[5, 6] = 685; Veer_divert[6, 6] = 686;
            Veer_divert[0, 7] = -1270; Veer_divert[1, 7] = -10; Veer_divert[2, 7] = -10; Veer_divert[3, 7] = 1269; Veer_divert[4, 7] = 1268; Veer_divert[5, 7] = 1980; Veer_divert[6, 7] = 1981;
            Veer_divert[0, 8] = 1247; Veer_divert[1, 8] = 1975; Veer_divert[2, 8] = 1974; Veer_divert[3, 8] = 1246; Veer_divert[4, 8] = 1245; Veer_divert[5, 8] = -10; Veer_divert[6, 8] = -10;
            Veer_divert[0, 9] = 1210; Veer_divert[1, 9] = 153; Veer_divert[2, 9] = 154; Veer_divert[3, 9] = 1209; Veer_divert[4, 9] = 1208; Veer_divert[5, 9] = 85; Veer_divert[6, 9] = 84;
            Veer_divert[0, 10] = 1127; Veer_divert[1, 10] = 1750; Veer_divert[2, 10] = 1751; Veer_divert[3, 10] = 1126; Veer_divert[4, 10] = 1125; Veer_divert[5, 10] = 1735; Veer_divert[6, 10] = 1734;
            Veer_divert[0, 11] = 960; Veer_divert[1, 11] = -10; Veer_divert[2, 11] = -10; Veer_divert[3, 11] = 959; Veer_divert[4, 11] = 958; Veer_divert[5, 11] = 2393; Veer_divert[6, 11] = 2392;
            Veer_divert[0, 12] = 930; Veer_divert[1, 12] = 1618; Veer_divert[2, 12] = 1617; Veer_divert[3, 12] = 929; Veer_divert[4, 12] = 928; Veer_divert[5, 12] = -10; Veer_divert[6, 12] = -10;
            Veer_divert[0, 13] = 890; Veer_divert[1, 13] = -10; Veer_divert[2, 13] = -10; Veer_divert[3, 13] = 889; Veer_divert[4, 13] = 888; Veer_divert[5, 13] = -10; Veer_divert[6, 13] = -10;
            Veer_divert[0, 14] = 1578; Veer_divert[1, 14] = 1693; Veer_divert[2, 14] = 1694; Veer_divert[3, 14] = 1577; Veer_divert[4, 14] = 1576; Veer_divert[5, 14] = 1645; Veer_divert[6, 14] = 1644;
            Veer_divert[0, 15] = 1285; Veer_divert[1, 15] = 540; Veer_divert[2, 15] = 539; Veer_divert[3, 15] = 540; Veer_divert[4, 15] = 539; Veer_divert[5, 15] = 540; Veer_divert[6, 15] = 541;
            Veer_divert[0, 16] = 1980; Veer_divert[1, 16] = 1270; Veer_divert[2, 16] = 1271; Veer_divert[3, 16] = -10; Veer_divert[4, 16] = -10; Veer_divert[5, 16] = 1266; Veer_divert[6, 16] = 1265;
            Veer_divert[0, 17] = 1760; Veer_divert[1, 17] = 1117; Veer_divert[2, 17] = 1118; Veer_divert[3, 17] = 1759; Veer_divert[4, 17] = 1758; Veer_divert[5, 17] = 1080; Veer_divert[6, 17] = 1079;
            Veer_divert[0, 18] = 1693; Veer_divert[1, 18] = 1510; Veer_divert[2, 18] = 1509; Veer_divert[3, 18] = 1692; Veer_divert[4, 18] = 1691; Veer_divert[5, 18] = 1578; Veer_divert[6, 18] = 1579;
            Veer_divert[0, 19] = -1693; Veer_divert[1, 19] = 1574; Veer_divert[2, 19] = 1575; Veer_divert[3, 19] = 1692; Veer_divert[4, 19] = 1691; Veer_divert[5, 19] = 1512; Veer_divert[6, 19] = 1511;
            Veer_divert[0, 19] = 1623; Veer_divert[1, 19] = 725; Veer_divert[2, 19] = 724; Veer_divert[3, 19] = 730; Veer_divert[4, 19] = 731; Veer_divert[5, 19] = 730; Veer_divert[6, 19] = 731;
            Veer_divert[0, 20] = 1280; Veer_divert[1, 20] = 490; Veer_divert[2, 20] = 491; Veer_divert[3, 20] = -10; Veer_divert[4, 20] = -10; Veer_divert[5, 20] = 489; Veer_divert[6, 20] = 488;

            if (Level == 3)
            {
                CarsMode[0] = 1; CarsMode[1] = 1;

            }
            else if (Level == 2)
            {
                CarsMode[0] = 1; CarsMode[1] = 1;

            }
            else if (Level == 3 || Level == 4)
            {
                CarsMode[0] = 1; CarsMode[1] = 1; CarsMode[2] = 1;

            }
            CarsRoute[0] = 2; CarsRoute[1] = 2; CarsRoute[2] = 2;
            modelPosition3 = Vector3.Zero;
            modelPosition0 = new Vector3(-450, 0, 1500);
            modelPosition1 = new Vector3(-900, 0, 400);
            modelPosition2 = new Vector3(0, 0, 0);
            modelPositionH = new Vector3(10000, 10000, 0);
            modelRotationH = 0;
            modelRotation1 = 0.0f;
            cam_position = new Vector3(2, 5, 0);



            graphics.SupportedOrientations = DisplayOrientation.LandscapeLeft | DisplayOrientation.LandscapeRight;


            proj = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, GraphicsDevice.Viewport.AspectRatio, 1.0f, 100.0f);
            proj3 = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, GraphicsDevice.Viewport.AspectRatio, 1.0f, 700.0f);
            view = Matrix.CreateLookAt(cam_position, new Vector3(0, 0, 0), Vector3.Up);
            viewH = Matrix.CreateLookAt(new Vector3(0, 0, 100), new Vector3(0, 0, 0f), Vector3.Up);
            view3 = Matrix.CreateLookAt(new Vector3(2, 45, -690), new Vector3(50, 35, 0), Vector3.Up);

            TouchPanel.EnabledGestures = GestureType.DoubleTap | GestureType.DragComplete |
                                         GestureType.Flick | GestureType.FreeDrag |
                                         GestureType.Hold | GestureType.HorizontalDrag |
                                         GestureType.Pinch | GestureType.PinchComplete |
                                         GestureType.Tap | GestureType.VerticalDrag;
            float z, x, sp = 40;
            int i = 0, j;
            x = 2700f; z = -1900f;
            float speed1 = 1;
            ///////// Road 1
            R1[2, 0] = -0.38f; R1[0, 0] = x; R1[1, 0] = z;
            for (j = 0; j < 37; j++) { i++; R1[2, i] = R1[2, i - 1]; z += 36; x -= 16; R1[0, i] = x; R1[1, i] = z; };

            R1[0, 37] = 10000; R1[1, 37] = 10000;
            for (j = 38; j < 70; j++) { i++; R1[0, j] = R1[0, j - 1] + 40; R1[1, j] = R1[1, j - 1] + 40; };
            x -= 1000; z += 1100; R1[0, 70] = R1[0, 69] + 40; R1[1, 70] = R1[1, 69] + 40;
            for (j = 0; j < 47; j++) { i++; R1[0, i] = x; R1[1, i] = z; R1[2, i] = -1.0f; z += 12.3f; x -= 21f; };
            for (j = 0; j < 25; j++) { i++; R1[0, i] = x; R1[1, i] = z; R1[2, i] = R1[2, i - 1] + .02f; z += 12.3f; x -= 22.5f; };

            speed1 = 1; sp = 40;
            while (z < 4600)
            { R1[0, i] = x; R1[1, i] = z; R1[2, i] = -0.25f; i++; z += (sp * speed1); x -= (7 * speed1); speed1 -= 0.0003f; };
            R1[0, 162] = 20000; R1[1, 162] = 20000;
            for (j = 163; j < 202; j++) { R1[0, j] = R1[0, j - 1] + 40; R1[1, j] = R1[1, j - 1] + 40; };
            // piche 1
            for (j = 0; j < 54; j++) { z += 9f; x += 1f; R1[0, i] = x; R1[1, i] = z; R1[2, i] = 0.019f + R1[2, i - 1]; i++; };
            for (j = 0; j < 54; j++) { z += 11f; x += 1f; R1[0, i] = x; R1[1, i] = z; R1[2, i] = 0.019f + R1[2, i - 1]; i++; };
            speed1 = 1;
            while (x < 1700)
            { R1[0, i] = x; R1[1, i] = z; R1[2, i] = R1[2, i - 1]; i++; z = R1[1, i - 1] - 7; x += (sp * speed1); speed1 -= 0.0003f; };

            // piche 2
            for (j = 0; j < 110; j++) { R1[0, i] = x; R1[1, i] = z; R1[2, i] = 0.012f + R1[2, i - 1]; z -= 9f; x += 12f; i++; };
            while (z > -3400)
            { R1[0, i] = x; R1[1, i] = z; R1[2, i] = R1[2, i - 1]; i++; z = R1[1, i - 1] - (sp * speed1); x = R1[0, i - 1]; speed1 -= 0.0003f; };
            speed1 = 1;

            // piche 3
            for (j = 0; j < 140; j++) { R1[0, i] = x; R1[1, i] = z; R1[2, i] = 0.015f + R1[2, i - 1]; z -= 13f; x -= 10f; i++; };
            while (x > -2400)
            { R1[0, i] = x; R1[1, i] = z; R1[2, i] = R1[2, i - 1]; i++; x = R1[0, i - 1] - (sp * speed1); z = R1[1, i - 1] + 18; speed1 -= 0.0006f; };
            speed1 = 1;

            //piche 4
            for (j = 0; j < 108; j++) { z += 10f; x += -1f; R1[0, i] = x; R1[1, i] = z; R1[2, i] = 0.015f + R1[2, i - 1]; i++; };
            for (j = 0; j < 223; j++) { z += 30f; x += 19.5f; R1[0, i] = x; R1[1, i] = z; R1[2, i] = R1[2, i - 1]; i++; };
            speed1 = 1;

            RoadPointAmount[0] = i - 1;


            //// Road 2
            x = 3000; z = 2765; R1[0, i] = x; R1[1, i] = z; R1[2, i] = 3.135f;
            for (j = 1; j < 55; j++)
            { i++; R1[0, i] = x; R1[1, i] = z; R1[2, i] = R1[2, i - 1] + 0.009f; z -= (20 * speed1); x -= (10 * speed1); };
            for (j = 55; j < 120; j++)
            { i++; R1[0, i] = x; R1[1, i] = z; R1[2, i] = R1[2, i - 1] + 0.0015f; z -= (20 * speed1); x -= (10 * speed1); speed1 -= 0.0003f; };
            for (j = 120; j < 205; j++)
            { i++; R1[0, i] = x; R1[1, i] = z; R1[2, i] = R1[2, i - 1] - 0.0015f; z -= (20 * speed1); x -= (8 * speed1); speed1 += 0.0003f; };
            speed1 = 1f;
            for (j = 205; j < 340; j++)
            { i++; R1[0, i] = x; R1[1, i] = z; R1[2, i] = R1[2, i - 1]; z -= (20 * speed1); x -= (10 * speed1); speed1 -= 0.0003f; };

            RoadPointAmount[1] = i - 1 - RoadPointAmount[0];

            //// Road 3
            x = 2500; z = -3300; R1[0, i] = x; R1[1, i] = z; R1[2, i] = -1.13f;
            speed1 = 1;
            for (j = 1; j < 90; j++)
            { i++; R1[2, i] = R1[2, i - 1]; z += (20 * speed1); x -= (40 * speed1); R1[0, i] = x; R1[1, i] = z; speed1 -= 0.0005f; };

            for (j = 0; j < 50; j++)
            { i++; R1[0, i] = x; R1[1, i] = z; R1[2, i] = R1[2, i - 1] + .012f; z += 12.3f; x -= 21f; };


            speed1 = 1f;
            for (j = 0; j < 37; j++)
            { i++; R1[0, i] = x; R1[1, i] = z; R1[2, i] = R1[2, i - 1] + 0.007f; z += 30f; x -= 7f; };
            for (j = 0; j < 37; j++)
            { i++; R1[0, i] = x; R1[1, i] = z; R1[2, i] = R1[2, i - 1] + 0.009f; z += 30f; x += 1f; };
            for (j = 0; j < 33; j++)
            { i++; R1[0, i] = x; R1[1, i] = z; R1[2, i] = R1[2, i - 1] + 0.013f; z += 27f; x += 13f; };
            for (j = 0; j < 30; j++)
            { i++; R1[0, i] = x; R1[1, i] = z; R1[2, i] = R1[2, i - 1] + 0.013f; z += 18f; x += 20f; };
            for (j = 0; j < 35; j++)
            { i++; R1[0, i] = x; R1[1, i] = z; R1[2, i] = R1[2, i - 1] + 0.013f; z += 16f; x += 20f; };
            for (j = 0; j < 45; j++)
            { i++; R1[0, i] = x; R1[1, i] = z; R1[2, i] = R1[2, i - 1] + 0.01f; z += 2f; x += 24f; };

            RoadPointAmount[2] = i - 1 - RoadPointAmount[1] - RoadPointAmount[0];



            //// Road 4
            x = 1700; z = 3550; R1[0, i] = x; R1[1, i] = z; R1[2, i] = -3.5f;
            speed1 = 1;
            for (j = 1; j < 40; j++)
            { i++; R1[2, i] = R1[2, i - 1] + 0.01f; z -= 20; x += 3.5f; R1[0, i] = x; R1[1, i] = z; };
            for (j = 1; j < 15; j++)
            { i++; R1[2, i] = R1[2, i - 1] + 0.03f; z -= 20; x -= 5f; R1[0, i] = x; R1[1, i] = z; };

            for (j = 1; j < 170; j++)
            { i++; R1[2, i] = R1[2, i - 1]; z -= 20; x -= 11.1f; R1[0, i] = x; R1[1, i] = z; };
            R1[0, 2080] = 30000; R1[1, 2080] = 30000;
            for (j = 2081; j < 2160; j++) { R1[0, j] = R1[0, j - 1] + 40; R1[1, j] = R1[1, j - 1] + 40; };

            RoadPointAmount[3] = i - 1 - RoadPointAmount[2] - RoadPointAmount[1] - RoadPointAmount[0];

            //// pomp benzin
            x = 600; z = -4600; R1[0, i] = x; R1[1, i] = z; R1[2, i] = 5.22f;
            speed1 = 1;
            for (j = 1; j < 50; j++)
            { i++; R1[2, i] = R1[2, i - 1] - 0.005f; z -= 15; x -= 15f; R1[0, i] = x; R1[1, i] = z; };

            for (j = 1; j < 100; j++)
            { i++; R1[2, i] = R1[2, i - 1] + 0.003f; z += 5; x -= 10f; R1[0, i] = x; R1[1, i] = z; };

            for (j = 1; j < 50; j++)
            { i++; R1[2, i] = R1[2, i - 1] + 0.02f; z += 20; x -= 10f; R1[0, i] = x; R1[1, i] = z; };



            RoadPointAmount[4] = i - 1 - RoadPointAmount[3] - RoadPointAmount[2] - RoadPointAmount[1] - RoadPointAmount[0];


            base.Initialize();

        }

        protected override void LoadContent()
        {
            jump = Content.Load<Texture2D>("jump5");
            circle = Content.Load<Texture2D>("circle");
            
            help = Content.Load<Texture2D>("help");
            levels_lock = Content.Load<Texture2D>("levels_lock");
            stopmusic = Content.Load<Texture2D>("stopmusic");
            back = Content.Load<Texture2D>("back");
            fullversion = Content.Load<Texture2D>("fullversion");
            ok = Content.Load<Texture2D>("ok");
            eatHeart = Content.Load<SoundEffect>("beep");
            beep_crash = Content.Load<SoundEffect>("beep-crash");
            beep_eat = Content.Load<SoundEffect>("beep-eat");
            beep_500 = Content.Load<SoundEffect>("beep_500");
            newgame = Content.Load<Texture2D>("newgame");
            soundOFcarAccident = Content.Load<SoundEffect>("soundofcaracc");
            hooray = Content.Load<SoundEffect>("hooray");
            carsound = Content.Load<Song>("ferrari");
            soundon = Content.Load<Texture2D>("soundon");
            soundoff = Content.Load<Texture2D>("soundoff");
            winner = Content.Load<Texture2D>("winner");
            spriteBatch = new SpriteBatch(GraphicsDevice);
            font = Content.Load<SpriteFont>("spritefont2");
            angleFont = Content.Load<SpriteFont>("spriteFont1");
            angleFont2 = Content.Load<SpriteFont>("spriteFont3");
            fall = Content.Load<Texture2D>("fall");
            wanet1_left = Content.Load<Texture2D>("wanet1_left");
            wanet1_grade = Content.Load<Texture2D>("wanet1_grade");
            wanet1_right = Content.Load<Texture2D>("wanet1_right");
            wanet2_left = Content.Load<Texture2D>("wanet2_left");
            wanet2_grade = Content.Load<Texture2D>("wanet2_grade");
            wanet2_right = Content.Load<Texture2D>("wanet2_right");
            wanet3_left = Content.Load<Texture2D>("wanet3_left");
            wanet3_grade = Content.Load<Texture2D>("wanet3_grade");
            wanet3_right = Content.Load<Texture2D>("wanet3_right");
            grade = Content.Load<Texture2D>("grade");
            continueSign = Content.Load<Texture2D>("continiue");
            left = Content.Load<Texture2D>("left");
            PausePic = Content.Load<Texture2D>("pause1");
            PlayPic = Content.Load<Texture2D>("play");
            right = Content.Load<Texture2D>("right");
            summer = Content.Load<Texture2D>("summer");
            winter = Content.Load<Texture2D>("winter");
            lambur = Content.Load<Texture2D>("royal2");
            hilux = Content.Load<Texture2D>("kreiz");
            gameOver = Content.Load<Texture2D>("fertig");
            levels = Content.Load<Texture2D>("levels2");
            SCORE = Content.Load<Texture2D>("SCORE");
            red1 = Content.Load<Texture2D>("red1"); red_1 = Content.Load<Texture2D>("red_1");
            red2 = Content.Load<Texture2D>("red2"); red_2 = Content.Load<Texture2D>("red_2");
            red3 = Content.Load<Texture2D>("red3"); red_3 = Content.Load<Texture2D>("red_3");
            red4 = Content.Load<Texture2D>("red4"); red_4 = Content.Load<Texture2D>("red_4");
            red5 = Content.Load<Texture2D>("red5"); red_5 = Content.Load<Texture2D>("red_5");
            green1 = Content.Load<Texture2D>("green1"); green_1 = Content.Load<Texture2D>("green_1");
            green2 = Content.Load<Texture2D>("green2"); green_2 = Content.Load<Texture2D>("green_2");
            green3 = Content.Load<Texture2D>("green3"); green_3 = Content.Load<Texture2D>("green_3");
            green4 = Content.Load<Texture2D>("green4"); green_4 = Content.Load<Texture2D>("green_4");
            green5 = Content.Load<Texture2D>("green5"); green_5 = Content.Load<Texture2D>("green_5");


            model0 = Content.Load<Model>("L200-fbx");
            foreach (ModelMesh mm in model0.Meshes)
            {
                foreach (Effect e in mm.Effects)
                {
                    IEffectLights iel = e as IEffectLights;
                    if (iel != null)
                    {
                        iel.EnableDefaultLighting();
                        iel.AmbientLightColor = Color.Yellow.ToVector3();
                    }
                }
            }
            model1 = Content.Load<Model>("L200_f");
            foreach (ModelMesh mm in model1.Meshes)
            {
                foreach (Effect e in mm.Effects)
                {
                    IEffectLights iel = e as IEffectLights;
                    if (iel != null)
                    {
                        iel.EnableDefaultLighting();
                        iel.AmbientLightColor = Color.Red.ToVector3();
                    }
                }
            }
            model2 = Content.Load<Model>("L200");
            foreach (ModelMesh mm in model2.Meshes)
            {
                foreach (Effect e in mm.Effects)
                {
                    IEffectLights iel = e as IEffectLights;
                    if (iel != null)
                    {
                        iel.EnableDefaultLighting();
                        iel.AmbientLightColor = Color.Blue.ToVector3();
                    }
                }
            }
            modelheart = Content.Load<Model>("heart");
            foreach (ModelMesh mm in modelheart.Meshes)
            {
                foreach (Effect e in mm.Effects)
                {
                    IEffectLights iel = e as IEffectLights;
                    if (iel != null)
                    {
                        iel.EnableDefaultLighting();
                        iel.AmbientLightColor = Color.Red.ToVector3();
                    }
                }
            }


        }


        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here

        }
        /// ///////////////////////////////////////////////////////////////////////
        private float tenXposition(int i)
        {
            if (CarsArrayPosition[i, 0] <= CarsArrayPosition[i, 1])
            {

                int j = 0;
                if ((CarsArrayPosition[i, 1] > 1308 && CarsArrayPosition[i, 1] < 1610) || (CarsArrayPosition[i, 1] > 1985 && CarsArrayPosition[i, 1] < 2198) || (CarsArrayPosition[i, 1] > 0 && CarsArrayPosition[i, 1] < 95))
                    j += 10;
                if (CarsArrayPosition[i, 1] + 30 + j > 2394)
                    return (R1[0, 2394]);
                return (R1[0, CarsArrayPosition[i, 1] + 30 + j]);
            }
            else
            {
                int j = 0;


                if ((CarsArrayPosition[i, 1] > 1308 && CarsArrayPosition[i, 1] < 1610) || (CarsArrayPosition[i, 1] > 1985 && CarsArrayPosition[i, 1] < 2198))
                    j += 10;
                if (CarsArrayPosition[i, 1] - 30 - j < 1)
                    return (R1[0, 0]);
                return (R1[0, CarsArrayPosition[i, 1] - 30 - j]);
            }
        }


        private float tenZposition(int i)
        {


            if (CarsArrayPosition[i, 0] <= CarsArrayPosition[i, 1])
            {
                int j = 0;
                if ((CarsArrayPosition[i, 1] > 1308 && CarsArrayPosition[i, 1] < 1610) || (CarsArrayPosition[i, 1] > 1985 && CarsArrayPosition[i, 1] < 2198) || (CarsArrayPosition[i, 1] > 0 && CarsArrayPosition[i, 1] < 95))
                    j += 10;
                if (CarsArrayPosition[i, 1] + 30 + j > 2394)
                    return (R1[1, 2394]);
                return (R1[1, CarsArrayPosition[i, 1] + 30 + j]);
            }
            else
            {
                int j = 0;
                if ((CarsArrayPosition[i, 1] > 1308 && CarsArrayPosition[i, 1] < 1610) || (CarsArrayPosition[i, 1] > 1985 && CarsArrayPosition[i, 1] < 2198))
                    j += 10;
                if (CarsArrayPosition[i, 1] - 30 - j < 1)
                    return (R1[1, 0]);
                return (R1[1, CarsArrayPosition[i, 1] - 30 - j]);
            }
        }
        ///////////////////////////////////////////////////////////////////////////
        int findNewWay_grade(int position)
        {
            for (int j = 0; j < 20; j++)
                if (position == Veer_grade[0, j] || position == (Veer_grade[0, j] - 1))
                    return (j);
            return (-1);
        }
        ///////////////////////////////////////////////////////////////////////////
        int findNewWay_divert(int position)
        {
            for (int j = 0; j < 21; j++)
                if (position == Veer_divert[0, j] || position == (Veer_divert[0, j] + 1))
                    return (j);
            return (-1);
        }
        //////////////////////////////////////////////////////////////////////////
        void changeRouteGrade(int i)
        {
            int k = -1;
            for (int j = 0; j < 17; j++)
                if (CarsArrayPosition[i, 1] == autoVeergrade[j] || CarsArrayPosition[i, 1] == autoVeergrade[j] + 1)
                    k = j;
            if (k > -1)
            {
                Random r = new Random(); int inc = (int)r.Next(1, 4);
                CarsRoute[i] = inc;

            }

        }
        ///////////////////////////////////////////////////////////////////////////

        void changeRouteDivert(int i)
        {
            int k = -1;
            for (int j = 0; j < 17; j++)
                if (CarsArrayPosition[i, 1] == autoVeerdivert[j] || CarsArrayPosition[i, 1] == autoVeerdivert[j] - 1)
                    k = j;
            if (k > -1)
            {
                Random r = new Random(); int inc = (int)r.Next(1, 4);
                CarsRoute[i] = inc;

            }

        }
        /// ///////////////////////////////////////////////////////////////////////
        private float nextXposition(int i)
        {
            if (CarsArrayPosition[i, 1] == 2320 || CarsArrayPosition[i, 1] == 2321)
            {
                parking[i]++;
                if (i == 0 && SOUND)
                    MediaPlayer.Stop();
                if (parking[i] < 150)
                    return (R1[0, CarsArrayPosition[i, 1]]);
                else
                {
                    parking[i] = 0;
                    if (i == 0 && SOUND && !PAUSE && !finish && !ampleSound)
                        MediaPlayer.Play(carsound);
                    if (!Divert[i] && CarsArrayPosition[i, 1] == 2320)
                    {
                        CarsArrayPosition[i, 0] = 2321;
                        CarsArrayPosition[i, 1] = 2322;
                        return (R1[0, CarsArrayPosition[i, 1]]);
                    }
                    else if (Divert[i] && CarsArrayPosition[i, 1] == 2321)
                    {
                        CarsArrayPosition[i, 0] = 2320;
                        CarsArrayPosition[i, 1] = 2319;
                        return (R1[0, CarsArrayPosition[i, 1]]);
                    }

                }

            }
            

            
            
            if (CarsArrayPosition[i, 0] <= CarsArrayPosition[i, 1])
            {
                Divert[i] = false;
                if ((CarsArrayPosition[i, 1] == 83 || CarsArrayPosition[i, 1] == 84 || CarsArrayPosition[i, 1] == 1145 || CarsArrayPosition[i, 1] == 1144) && !ampel1 && !Divert[i])
                {
                    if (i == 0 && !ampleSound && SOUND)
                    {
                        MediaPlayer.Stop();
                        ampleSound = true;
                    }
                    return (R1[0, CarsArrayPosition[i, 1]]);
                }
                if ((CarsArrayPosition[i, 1] == 1483 || CarsArrayPosition[i, 1] == 1482 || CarsArrayPosition[i, 1] == 1645 || CarsArrayPosition[i, 1] == 1644) && !ampel2 && !Divert[i])
                {
                    if (i == 0 && !ampleSound && SOUND)
                    {
                        MediaPlayer.Stop();
                        ampleSound = true;
                    }
                    return (R1[0, CarsArrayPosition[i, 1]]);
                }
                if (i == 0 && ampleSound && SOUND && !PAUSE && !finish)
                {
                    MediaPlayer.Play(carsound); ampleSound = false;
                }
                if ((CarsArrayPosition[i, 1] > 1780 && CarsArrayPosition[i, 1] < 1970) || (CarsArrayPosition[i, 1] > 160 && CarsArrayPosition[i, 1] < 396) || (CarsArrayPosition[i, 1] > 902 && CarsArrayPosition[i, 1] < 1280))
                    leftORright[i] = false;
                else leftORright[i] = true;
                if (!finish && i > 0 && !PAUSE)
                    changeRouteGrade(i);
                int newway = findNewWay_grade(CarsArrayPosition[i, 1]);
                if (newway == -1)
                {
                    CarsArrayPosition[i, 0] = CarsArrayPosition[i, 1];
                    CarsArrayPosition[i, 1]++;
                }
                else
                {
                    switch (CarsRoute[i])
                    {
                        case 1: { if (Veer_grade[1, newway] == -10) { CarsArrayPosition[i, 0] = Veer_grade[3, newway]; CarsArrayPosition[i, 1] = Veer_grade[4, newway]; return (R1[0, CarsArrayPosition[i, 1]]); } CarsArrayPosition[i, 0] = Veer_grade[1, newway]; CarsArrayPosition[i, 1] = Veer_grade[2, newway]; CarsRoute[i] = 2; return (R1[0, CarsArrayPosition[i, 1]]); }
                        case 2: { if (Veer_grade[3, newway] == -10) { CarsArrayPosition[i, 0] = Veer_grade[1, newway]; CarsArrayPosition[i, 1] = Veer_grade[2, newway]; return (R1[0, CarsArrayPosition[i, 1]]); } CarsArrayPosition[i, 0] = Veer_grade[3, newway]; CarsArrayPosition[i, 1] = Veer_grade[4, newway]; CarsRoute[i] = 2; return (R1[0, CarsArrayPosition[i, 1]]); }
                        case 3: { if (Veer_grade[5, newway] == -10) { CarsArrayPosition[i, 0] = Veer_grade[3, newway]; CarsArrayPosition[i, 1] = Veer_grade[4, newway]; return (R1[0, CarsArrayPosition[i, 1]]); } CarsArrayPosition[i, 0] = Veer_grade[5, newway]; CarsArrayPosition[i, 1] = Veer_grade[6, newway]; CarsRoute[i] = 2; return (R1[0, CarsArrayPosition[i, 1]]); }
                    }
                }
                int X = CarsArrayPosition[i, 1];
                if ((X > 0 && X < 10) || (X > 70 && X < 94) || (X > 340 && X < 375) || (X > 490 && X < 690) || (X > 830 && X < 880) || (X > 885 && X < 910) ||
                    (X > 975 && X < 1060) || (X > 1127 && X < 1143) || (X > 1210 && X < 1245) || (X > 1340 && X < 1480) ||
                    (X > 1620 && X < 1640) || (X > 1686 && X < 1710) || (X > 1850 && X < 1900) || (X > 1985 && X < 2020) || (X > 2015 && X < 2155))
                    SW[i] = true;
                else SW[i] = false;
                if ((CarsArrayPosition[i, 1] > 1308 && CarsArrayPosition[i, 1] < 1480))
                { CarsArrayPosition[i, 0] = CarsArrayPosition[i, 1]; CarsArrayPosition[i, 1]++; }

                else if (Speed == 2 && (CarsArrayPosition[i, 1] < 1623 || CarsArrayPosition[i, 1] > 1725))
                {

                    if (RANDOM == 1)
                    { CarsArrayPosition[i, 0] = CarsArrayPosition[i, 1]; CarsArrayPosition[i, 1] += 1; }
                }
                else if (Level == 4)
                {

                    if (RANDOM == 1 || RANDOM == 2)
                    { CarsArrayPosition[i, 0] = CarsArrayPosition[i, 1]; CarsArrayPosition[i, 1] += 1; }
                }


                return (R1[0, CarsArrayPosition[i, 1]]);
            }
            else
            {
                if (!finish && i > 0 && !PAUSE)
                    changeRouteDivert(i);
                Divert[i] = true;
                if ((CarsArrayPosition[i, 1] == 153 || CarsArrayPosition[i, 1] == 154 || CarsArrayPosition[i, 1] == 1215 || CarsArrayPosition[i, 1] == 1216) && !ampel1 && Divert[i])
                {
                    if (i == 0 && !ampleSound && SOUND)
                    {
                        MediaPlayer.Stop();
                        ampleSound = true;
                    }
                    return (R1[0, CarsArrayPosition[i, 1]]);
                }
                if ((CarsArrayPosition[i, 1] == 1602 || CarsArrayPosition[i, 1] == 1603 || CarsArrayPosition[i, 1] == 1693 || CarsArrayPosition[i, 1] == 1694) && !ampel2 && Divert[i])
                {
                    if (i == 0 && !ampleSound && SOUND)
                    {
                        MediaPlayer.Stop();
                        ampleSound = true;
                    }
                    return (R1[0, CarsArrayPosition[i, 1]]);
                }
                if (i == 0 && ampleSound && SOUND && !PAUSE && !finish)
                {
                    MediaPlayer.Play(carsound); ampleSound = false;
                }
                if ((CarsArrayPosition[i, 1] > 1623 && CarsArrayPosition[i, 1] < 1970) || (CarsArrayPosition[i, 1] > 0 && CarsArrayPosition[i, 1] < 330) || (CarsArrayPosition[i, 1] > 1285 && CarsArrayPosition[i, 1] < 1480)
                    || (CarsArrayPosition[i, 1] > 571 && CarsArrayPosition[i, 1] < 970) || (CarsArrayPosition[i, 1] > 2198 && CarsArrayPosition[i, 1] < 2394))
                    leftORright[i] = false;
                else leftORright[i] = true;
                int newway = findNewWay_divert(CarsArrayPosition[i, 1]);
                if (newway == -1)
                {
                    CarsArrayPosition[i, 0] = CarsArrayPosition[i, 1];
                    CarsArrayPosition[i, 1]--;
                }
                else
                {
                    switch (CarsRoute[i])
                    {
                        case 1: { if (Veer_divert[1, newway] == -10) { CarsArrayPosition[i, 0] = Veer_divert[3, newway]; CarsArrayPosition[i, 1] = Veer_divert[4, newway]; return (R1[0, CarsArrayPosition[i, 1]]); } CarsArrayPosition[i, 0] = Veer_divert[1, newway]; CarsArrayPosition[i, 1] = Veer_divert[2, newway]; CarsRoute[i] = 2; return (R1[0, CarsArrayPosition[i, 1]]); }
                        case 2: { if (Veer_divert[3, newway] == -10) { CarsArrayPosition[i, 0] = Veer_divert[1, newway]; CarsArrayPosition[i, 1] = Veer_divert[2, newway]; return (R1[0, CarsArrayPosition[i, 1]]); } CarsArrayPosition[i, 0] = Veer_divert[3, newway]; CarsArrayPosition[i, 1] = Veer_divert[4, newway]; CarsRoute[i] = 2; return (R1[0, CarsArrayPosition[i, 1]]); }
                        case 3: { if (Veer_divert[5, newway] == -10) { CarsArrayPosition[i, 0] = Veer_divert[3, newway]; CarsArrayPosition[i, 1] = Veer_divert[4, newway]; return (R1[0, CarsArrayPosition[i, 1]]); } CarsArrayPosition[i, 0] = Veer_divert[5, newway]; CarsArrayPosition[i, 1] = Veer_divert[6, newway]; CarsRoute[i] = 2; return (R1[0, CarsArrayPosition[i, 1]]); }
                    }
                }
                int X = CarsArrayPosition[i, 1];
                if ((X < 2198 && X > 2160) || (X < 2090 && X > 2040) || (X < 1900 && X > 1790) || (X < 1725 && X > 1695)
                    || (X < 1610 && X > 1603) || (X < 1480 && X > 1350) || (X < 1280 && X > 1220) || (X < 1175 && X > 1120) || (X < 970 && X > 955)
                    || (X < 780 && X > 535) || (X < 280 && X > 235) || (X < 50 && X > 30))
                    SW[i] = true;
                else SW[i] = false;


                if ((CarsArrayPosition[i, 1] > 1308 && CarsArrayPosition[i, 1] < 1480))
                { CarsArrayPosition[i, 0] = CarsArrayPosition[i, 1]; CarsArrayPosition[i, 1]--; }
                else if (Speed == 2 && (CarsArrayPosition[i, 1] < 1623 || CarsArrayPosition[i, 1] > 1725))
                {

                    if (RANDOM == 1)
                    { CarsArrayPosition[i, 0] = CarsArrayPosition[i, 1]; CarsArrayPosition[i, 1] -= 1; }
                }
                else if (Level == 4 && (RANDOM == 1 || RANDOM == 2))
                { CarsArrayPosition[i, 0] = CarsArrayPosition[i, 1]; CarsArrayPosition[i, 1]--; }
                return (R1[0, CarsArrayPosition[i, 1]]);
            }
        }
        private float nextZposition(int i)
        {
            if (CarsArrayPosition[i, 0] <= CarsArrayPosition[i, 1])
            {

                return (R1[1, CarsArrayPosition[i, 1]]);
            }
            else
            {

                return (R1[1, CarsArrayPosition[i, 1]]);
            }
        }
        private float nextRotation(int i)
        {
            if (CarsArrayPosition[i, 0] <= CarsArrayPosition[i, 1])
            {

                return (R1[2, CarsArrayPosition[i, 1]]);
            }
            else
            {

                return (R1[2, CarsArrayPosition[i, 1]] + 3.14f);
            }
        }

        /// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        void set_page1(float x, float y)
        {
            if (y >= 390 && y < 460 && x >= 252 && x < 467 && !Unfinished)
            { page = 2;  }
            else if (y >= 390 && y < 460 && x >= 252 && x < 467 && Unfinished)
            { readMAX(Level); page = 3; }
            else if (y >= 346 && y < 396 && x >= 252 && x < 467 && Unfinished)
            { page = 2; RECORD = 0; sw_acc = true; }
            else if (y > 0 && y < 55 && x > 700 && x < 800 && Unfinished)
            {
                if (MediaPlayer.GameHasControl)
                    SOUND = !SOUND;
                else SW_stopBG = true;
            }
            return;
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        void writeMAX(int i)
        {
            if (MAX >= RECORD)
                return;
            using (IsolatedStorageFile file = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (i == 1)
                { using (BinaryWriter writer = new BinaryWriter(file.CreateFile("maxLevel1"))) writer.Write(RECORD); }
                else if (i == 2)
                { using (BinaryWriter writer = new BinaryWriter(file.CreateFile("maxLevel2"))) writer.Write(RECORD); }
                else if (i == 3)
                { using (BinaryWriter writer = new BinaryWriter(file.CreateFile("maxLevel3"))) writer.Write(RECORD); }
                else if (i == 4)
                { using (BinaryWriter writer = new BinaryWriter(file.CreateFile("maxLevel4"))) writer.Write(RECORD); }


            }

        }


        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        void readMAX(int i)
        {
            using (IsolatedStorageFile file = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (i == 1)
                {
                    using (BinaryReader reader = new BinaryReader(file.OpenFile("maxLevel1", FileMode.OpenOrCreate))) if (reader.BaseStream.Length > 0) MAX = reader.ReadInt64();
                }
                else if (i == 2)
                {
                    using (BinaryReader reader = new BinaryReader(file.OpenFile("maxLevel2", FileMode.OpenOrCreate))) if (reader.BaseStream.Length > 0) MAX = reader.ReadInt64();
                }
                else if (i == 3)
                {
                    using (BinaryReader reader = new BinaryReader(file.OpenFile("maxLevel3", FileMode.OpenOrCreate))) if (reader.BaseStream.Length > 0) MAX = reader.ReadInt64();
                }
                else if (i == 4)
                {
                    using (BinaryReader reader = new BinaryReader(file.OpenFile("maxLevel4", FileMode.OpenOrCreate))) if (reader.BaseStream.Length > 0) MAX = reader.ReadInt64();
                }

            }
        }
        /// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        void set_levels(float x, float y)
        {
            if (y >= 373  && y < 460)
            {
                if (x >= 200 && x < 334)
                {
                    if (sw_acc) { Boolean Sou = SOUND; Initialize(); SOUND = Sou; sw_acc = false; } readMAX(1); Level = 1; Speed = 1; page = 3;
                }
                else if (x >= 334 && x < 464 && !Guide.IsTrialMode)
                { if (sw_acc) { Boolean Sou = SOUND; Initialize(); SOUND = Sou; sw_acc = false; } readMAX(2); Level = 2; Speed = 2; page = 3; }
                else if (x >= 464 && x <= 609 && !Guide.IsTrialMode)
                { if (sw_acc) { Boolean Sou = SOUND; Initialize(); SOUND = Sou; sw_acc = false; } readMAX(3); Level = 3; Speed = 2; page = 3; }
                //else if (x >= 545 && x <= 663)
                //{ if (sw_acc) { Boolean Sou = SOUND; Initialize(); SOUND = Sou; sw_acc = false; } readMAX(4); Level = 4; Speed = 3; page = 3; }
            }
            STEPS[0, 0] = STEPS[0, 1] = STEPS[0, 2] = -1;
            STEPS[1, 0] = STEPS[1, 1] = STEPS[1, 2] = -1;
            sw_a = sw_b = sw_c = false;
            stepsSW = false;
            stepsCounter = 0;
            return;
        }
        /////////////////////////////////////////////////////////////////
        void stepsRandomCreator()
        {
            //rd = 1;
             if (rd == 1)
            {
                STEPS[0, 0] = 75; STEPS[0, 1] = 260; STEPS[0, 2] = 590;
                STEPS[1, 0] = 400; STEPS[1, 1] = 100; STEPS[1, 2] = 310;
            }
            else if (rd == 2)
            {
                STEPS[0, 2] = 75; STEPS[0, 0] = 260; STEPS[0, 1] = 590;
                STEPS[1, 2] = 400; STEPS[1, 0] = 100; STEPS[1, 1] = 310;
            }
            else if (rd == 3)
            {
                STEPS[0, 1] = 75; STEPS[0, 0] = 260; STEPS[0, 2] = 590;
                STEPS[1, 1] = 400; STEPS[1, 0] = 100; STEPS[1, 2] = 310;
            }
        }
        void resetSteps()
        {
            stepsSW = false;
            stepsCounter = 0;
            STEPS[0, 0] = STEPS[0, 1] = STEPS[0, 2] = -1;
            STEPS[1, 0] = STEPS[1, 1] = STEPS[1, 2] = -1;
            sw_a = sw_b = sw_c = false;

        }
        /// /////////////////////////////////////////////////////////////
        void checkFORgift(int i)
        {
            switch (i)
            {
                case 0: {return ; }
                case 1: { 
                          if(STEPS[0,0]==0) return;
                          resetSteps();
                          if (SOUND)  beep_crash.Play(1.0f, 0.0f, 0.0f);
                           break; 
                        }
                case 2: {
                        if (STEPS[0, 0] == 0 && STEPS[0, 1] == 0)
                        {
                            if (SOUND) beep_500.Play(1.0f, 0.0f, 0.0f);
                            //if (SOUND) eatHeart.Play(2.0f, 0.0f, 0.0f);
                            //if (SOUND) eatHeart.Play(2.0f, 0.0f, 0.0f);
                            resetSteps();
                            RECORD += 500;
                        }
                        else
                        {
                            resetSteps();
                            if (SOUND) beep_crash.Play(1.0f, 0.0f, 0.0f);
                        }
                        break;
                      }
                
            }
            
        }

/////////////////////////////////////////////////////////////////////////
        protected override void Update(GameTime gameTime)
        {
           // if (GamePad.GetState(PlayerIndex.One).Buttons.A == ButtonState.Pressed)
               // this.Exit();
            if (!finish && !PAUSE)
            stepsCounter++;
            if (stepsCounter == 500)
            {
                
                Random r2 = new Random(); rd = (int)r2.Next(1, 4);
                stepsRandomCreator();
                stepsSW = true;
                sw_a = sw_b = sw_c = true;
            }
            if (stepsCounter == 1800)
            
                resetSteps();
            
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed && page == 1)
                this.Exit();
            else if (page == 5 && GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                page = 2;
            else if (page == 2 && GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            { if (Unfinished) Unfinished = false; page = 1; }
            else if (page == 3 && GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed && (accident || WIN))
            {
                sw_acc = true;
                page = 2;
                return;
            }
            else if (page == 3 && GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed && !backORexit)
            {
                backORexit = true;
                oldpause = PAUSE;
                PAUSE = true;
            }
            else if (page == 3 && GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed && backORexit)
            {
                PAUSE = oldpause;
                //PAUSE =false;
                if (!PAUSE && SOUND && !ampleSound && !finish)
                    MediaPlayer.Resume();
                backORexit = false;
            }
            


            Random r = new Random(); RANDOM = (int)r.Next(0, 2);
            if (!finish && !PAUSE)
                heart_counter++;
            if (heart_counter == 600 && page == 3 && !finish && Level != 1 && !PAUSE)
            {
                Random r2 = new Random(); int inc = (int)r2.Next(2, 10);
                modelPositionH.X = hitHeart[0, inc];
                modelPositionH.Y = hitHeart[1, inc];
                heart_counter = 0; heartAccident = false;
                pixelPositionH = graphics.GraphicsDevice.Viewport.Project(modelPositionH, proj, viewH, Matrix.CreateScale(0.1f));
                RECORD += 10;
                if (Speed == 3)
                    RECORD += 5;
            }
            else if (heart_counter == 750 && page == 3 && !finish && Level == 1 && !PAUSE)
            {
                Random r2 = new Random(); int inc = (int)r2.Next(2, 10);
                modelPositionH.X = hitHeart[0, inc];
                modelPositionH.Y = hitHeart[1, inc];
                heart_counter = 0; heartAccident = false;
                pixelPositionH = graphics.GraphicsDevice.Viewport.Project(modelPositionH, proj, viewH, Matrix.CreateScale(0.1f));
                RECORD += 10;
            }
            if (heart_counter > 750)
                heart_counter = 340;

            if (!finish && !PAUSE && page == 3)
            { frame_counter--; frame_counter2--; }
            if (frame_counter == 0)
            {
                frame_counter = 600;
                if (ampel1)
                    ampel1 = false;
                else ampel1 = true;


            }
            if (frame_counter2 == 0)
            {
                frame_counter2 = 600;

                if (ampel2)
                    ampel2 = false;
                else ampel2 = true;
            }
            
            
            if (page == 3 && !finish && !PAUSE)
            {

                Vector3 pos = modelPosition0; pos.X = tenXposition(0); pos.Z = tenZposition(0);
                modelPosition0.X = nextXposition(0); modelPosition0.Z = nextZposition(0);
                modelRotation0 = nextRotation(0);
                modelPosition1.X = nextXposition(1); modelPosition1.Z = nextZposition(1);
                modelRotation1 = nextRotation(1);
                pixelPosition0 = graphics.GraphicsDevice.Viewport.Project(modelPosition0, proj, view, Matrix.CreateScale(0.0006f));
                pos0 = graphics.GraphicsDevice.Viewport.Project(pos, proj, view, Matrix.CreateScale(0.0006f));
                pos = modelPosition1; pos.X = tenXposition(1); pos.Z = tenZposition(1);
                pos1 = graphics.GraphicsDevice.Viewport.Project(pos, proj, view, Matrix.CreateScale(0.0006f));
                if ((Level == 3 || Level == 4) && !finish && !PAUSE)
                {
                    modelPosition2.X = nextXposition(2); modelPosition2.Z = nextZposition(2);
                    modelRotation2 = nextRotation(2);
                    pos = modelPosition2; pos.X = tenXposition(2); pos.Z = tenZposition(2);
                    pos2 = graphics.GraphicsDevice.Viewport.Project(pos, proj, view, Matrix.CreateScale(0.0006f));
                }

            }
            
            if (sw_a && page == 3  && pixelPosition0.X - 30 < 75 && 75 < pixelPosition0.X + 30 && pixelPosition0.Y - 30 < 400 && 400 < pixelPosition0.Y + 30 && !finish && !PAUSE)
            {   
                int G = 0;
                for (; G < 3; G++)
                    if (STEPS[0, G] == 75)
                    { STEPS[0, G] = 0; break; }
                if (SOUND)
                    beep_eat.Play(1.0f, 0.0f, 0.0f);
                sw_a = false;
                //STEPS[0, 0] = 0;
                checkFORgift(G);
                //heart_counter = 450;
                //modelPositionH.X = 50000;
                //RECORD += 100;
            }
            if (sw_b && page == 3 && pixelPosition0.X - 40 < 260 && 260 < pixelPosition0.X + 40 && pixelPosition0.Y - 40 <100 && 100 < pixelPosition0.Y + 40 && !finish && !PAUSE)
            {
                int G = 0;
                for (; G < 3; G++)
                    if (STEPS[0, G] == 260)
                    { STEPS[0, G] = 0; break; }
                if (SOUND)
                    beep_eat.Play(1.0f, 0.0f, 0.0f);
                sw_b = false;
                //STEPS[0, 1] = 0;
                checkFORgift(G);
                //heart_counter = 450;
                //modelPositionH.X = 50000;
                //RECORD += 100;
            }
            if (sw_c && page == 3 && pixelPosition0.X - 30< 590 && 590 < pixelPosition0.X + 30 && pixelPosition0.Y - 30 < 320 && 320 < pixelPosition0.Y + 30 && !finish && !PAUSE)
            {
                int G = 0;
                for (; G < 3; G++)
                    if (STEPS[0, G] == 590)
                    { STEPS[0, G] = 0; break; }
                if (SOUND)
                    beep_eat.Play(1.0f, 0.0f, 0.0f);
                sw_c = false;
                //STEPS[0, 2] = 0;
                checkFORgift(G);
                //heart_counter = 450;
                //modelPositionH.X = 50000;
                //RECORD += 100;
            }
            if (Level > 1 && page == 3 && heart_counter < 450 && pixelPosition0.X - 20 < pixelPositionH.X && pixelPositionH.X < pixelPosition0.X + 20 && pixelPosition0.Y - 20 < pixelPositionH.Y && pixelPositionH.Y < pixelPosition0.Y + 20 && !finish && !PAUSE)
            {
                heartAccident = true;
                if (SOUND )
                    eatHeart.Play(1.0f, 0.0f, 0.0f);
                heart_counter = 450;
                modelPositionH.X = 50000;
                RECORD += 100;
            }
            else if (Level == 1 && page == 3 && heart_counter < 650 && pixelPosition0.X - 20 < pixelPositionH.X && pixelPositionH.X < pixelPosition0.X + 20 && pixelPosition0.Y - 20 < pixelPositionH.Y && pixelPositionH.Y < pixelPosition0.Y + 20 && !finish && !PAUSE)
            {
                heartAccident = true;
                if (SOUND )
                    eatHeart.Play(1.0f, 0.0f, 0.0f);

                heart_counter = 650;
                modelPositionH.X = 50000;
                RECORD += 100;
            }

            if (RECORD >= maxFORwin)
            {
                WIN = true;
                writeMAX(Level);
                if (SOUND && page==3)
                MediaPlayer.Stop();
                if (SOUND && !finish)
                    hooray.Play(1.0f, 0.0f, 0.0f);
                finish = true;
            }
            if ((modelPosition1.X - 250 < R1[0, CarsArrayPosition[0, 0]] && R1[0, CarsArrayPosition[0, 0]] < modelPosition1.X + 250 && modelPosition1.Z - 250 < R1[1, CarsArrayPosition[0, 0]] && R1[1, CarsArrayPosition[0, 0]] < modelPosition1.Z + 250)
                 || (modelPosition1.X - 250 < modelPosition0.X && modelPosition0.X < modelPosition1.X + 250 && modelPosition1.Z - 250 < modelPosition0.Z && modelPosition0.Z < modelPosition1.Z + 250)
                 || (R1[0, CarsArrayPosition[1, 0]] - 250 < modelPosition0.X && modelPosition0.X < R1[0, CarsArrayPosition[1, 0]] + 250 && R1[1, CarsArrayPosition[1, 0]] - 250 < modelPosition0.Z && modelPosition0.Z < R1[1, CarsArrayPosition[1, 0]] + 250)

                && !finish && !PAUSE)
            {
                accident = true;
                writeMAX(Level);
                if (SOUND && page == 3)
                MediaPlayer.Stop();
                if (SOUND && !finish )
                    soundOFcarAccident.Play(1.0f, 0.0f, 0.0f);
                finish = true;


            }

            if ((modelPosition2.X - 250 < R1[0, CarsArrayPosition[0, 0]] && R1[0, CarsArrayPosition[0, 0]] < modelPosition2.X + 250 && modelPosition2.Z - 250 < R1[1, CarsArrayPosition[0, 0]] && R1[1, CarsArrayPosition[0, 0]] < modelPosition2.Z + 250)
                || (modelPosition2.X - 250 < modelPosition0.X && modelPosition0.X < modelPosition2.X + 250 && modelPosition2.Z - 250 < modelPosition0.Z && modelPosition0.Z < modelPosition2.Z + 250)
                || (R1[0, CarsArrayPosition[2, 0]] - 250 < modelPosition0.X && modelPosition0.X < R1[0, CarsArrayPosition[2, 0]] + 250 && R1[1, CarsArrayPosition[2, 0]] - 250 < modelPosition0.Z && modelPosition0.Z < R1[1, CarsArrayPosition[2, 0]] + 250)

                && (Level == 3 || Level == 4) && !finish && !PAUSE)
            {
                accident = true;
                writeMAX(Level);
                if (SOUND && page == 3)
                MediaPlayer.Stop();
                if (SOUND && !finish )
                    soundOFcarAccident.Play(1.0f, 0.0f, 0.0f);
                finish = true;


            }

            TouchPanel.EnabledGestures = GestureType.Tap | GestureType.FreeDrag;

            while (TouchPanel.IsGestureAvailable)
            {
                GestureSample gesture = TouchPanel.ReadGesture();
                
                switch (gesture.GestureType)
                {
                    case GestureType.Tap:
                        {

                            float y = gesture.Position.Y; float x = gesture.Position.X;
                            if ((SW_stopBG && page == 2) || (SW_stopBG && page == 1 && Unfinished))
                            {
                                if (y >=240  && y < 320 )
                                {
                                    if (x >= 230 && x < 350)
                                    {SOUND = false; SW_stopBG=false;}
                                    else if (x >= 370 && x < 495)
                                    { SOUND = true; SW_stopBG = false; MediaPlayer.Stop(); 
                                    }
                                    
                                }
                            }
                            else if (page == 1)
                            {
                                set_page1(x, y);
                            }
                            else if (page == 2)
                            {
                                if (y > 0 && y < 65 && x > 690 && x < 800)
                                {
                                    if (MediaPlayer.GameHasControl)
                                        SOUND = !SOUND;
                                    else SW_stopBG = true;
                                }
                                else if (y > 0 && y < 65 && x > 0 && x < 110)
                                {
                                    page = 5;
                                }
                                else set_levels(x, y);


                            }
                            else if (page == 5 && (y >= 390 && y < 460 && x >= 252 && x < 467))
                                page = 2;
                            else if (page == 3)
                            {
                                if (y >= 430 && y < 480 && x >= 0 && x < 55 && !PAUSE && !backORexit)
                                {
                                    if (CarsArrayPosition[0, 0] <= CarsArrayPosition[0, 1] && CarsArrayPosition[0, 1] < 1980)
                                    {
                                        if (CarsArrayPosition[0, 1] >1900)
                                        {
                                            CarsArrayPosition[0, 0] = 1247; CarsArrayPosition[0, 1] = 1248;
                                        }
                                        else if (CarsArrayPosition[0, 1] > 1540 && CarsArrayPosition[0, 1]<1620)
                                        {
                                            CarsArrayPosition[0, 0] = 930; CarsArrayPosition[0, 1] = 931;
                                        }
                                        else if (CarsArrayPosition[0, 1] > 1209 && CarsArrayPosition[0, 1] < 1280)
                                        {
                                            CarsArrayPosition[0, 0] = 430; CarsArrayPosition[0, 1] = 431;
                                        }
                                    else{
                                        CarsArrayPosition[0, 0] += 70; CarsArrayPosition[0, 1] += 70;
                                        }
                                    }
                                    if (CarsArrayPosition[0, 0] > CarsArrayPosition[0, 1] && CarsArrayPosition[0, 1] > 0 && CarsArrayPosition[0, 1] <1977)
                                    {
                                        if (CarsArrayPosition[0, 1] < 70)
                                        {
                                            CarsArrayPosition[0, 0] = 715; CarsArrayPosition[0, 1] = 716;
                                        }
                                        else if (CarsArrayPosition[0, 1] <1695 && CarsArrayPosition[0, 1] > 1623)
                                        {
                                            CarsArrayPosition[0, 0] = 730; CarsArrayPosition[0, 1] = 731;
                                        }
                                        else if (CarsArrayPosition[0, 1] <1380  && CarsArrayPosition[0, 1] >1308 )
                                        {
                                            CarsArrayPosition[0, 0] = 540 ; CarsArrayPosition[0, 1] = 539 ;
                                        }
                                        else
                                        {
                                            CarsArrayPosition[0, 0] -= 70; CarsArrayPosition[0, 1] -= 70;
                                        }
                                    }

                                }
                                if (y >= 0 && y < 140 && !PAUSE && !backORexit)
                                {
                                    if (x >= 630 && x < 700)
                                    {
                                        upDown = 3;
                                        CarsRoute[0] = Left();
                                    }
                                    else if (x >= 700 && x < 770 && y<=70)
                                    {
                                        upDown = 1;
                                        CarsRoute[0] = up();
                                    }
                                    else if (x >= 700 && x < 770 && y > 70)
                                    {
                                        upDown = 2;
                                        CarsRoute[0] = down();
                                    }
                                    else if (x >= 770 && x <= 800)
                                    {
                                        upDown = 4;
                                        CarsRoute[0] = Right();
                                    }
                                   
                                }
                                if (y >= 4 && y < 63 && !PAUSE && !backORexit)
                                {
                                    
                                     if (x >= 55 && x < 130)
                                        CarsRoute[1] = 1;
                                    else if (x >= 130 && x < 203)
                                        CarsRoute[1] = 2;
                                    else if (x >= 203 && x <= 280)
                                        CarsRoute[1] = 3;
                                }
                                else if (y >= 63 && y <= 115 && !PAUSE && !backORexit)
                                {
                                    if (x >= 55 && x < 130)
                                        CarsRoute[2] = 1;
                                    else if (x >= 130 && x < 203)
                                        CarsRoute[2] = 2;
                                    else if (x >= 203 && x <= 280)
                                        CarsRoute[2] = 3;
                                }
                                if (y > 440 && y < 480 && x > 760 && x < 800 && !backORexit)
                                {
                                    PAUSE = !PAUSE;
                                    if (!PAUSE && !ampleSound && SOUND)
                                        MediaPlayer.Resume();
                                }
                                if ((WIN) && (y >= 244 && y < 331))
                                {
                                    
                                    if (x >= 403 && x < 547)
                                    {
                                        if (Guide.IsTrialMode)
                                        { Guide.ShowMarketplace(PlayerIndex.One); }
                                        sw_acc = true;
                                        page = 2;
                                        return;
                                    }
                                    else if (x >= 263 && x < 402)
                                        this.Exit();
                                }
                                if ((accident ) && (y >= 244 && y < 331 ))
                                {
                                    if (x >= 403 && x < 547)
                                    {
                                        sw_acc = true;
                                        page = 2;
                                        return;
                                    }
                                    else if(x >= 263 && x < 402)
                                        this.Exit();
                                }
                                if (backORexit)
                                {

                                    if (x >= 295 && x < 505)
                                    {
                                        if (y >= 140 && y < 236)
                                        {
                                            PAUSE = oldpause;
                                            //PAUSE = false;
                                            if (!PAUSE && SOUND && !ampleSound && !finish)
                                                MediaPlayer.Resume();
                                            backORexit = false;
                                        }
                                        else if (y >= 236 && y < 320)
                                            this.Exit();
                                        else if (y >= 320 && y <= 435)
                                        { PAUSE = false; backORexit = false; sw_acc = true; page = 2; }

                                    }

                                }

                            }
                            break;
                        }

                }
            }



            base.Update(gameTime);
        }

        private float atan(int p)
        {
            throw new NotImplementedException();

        }

        /// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        int up()
        {

            if ((CarsArrayPosition[0, 1] >= 0 && CarsArrayPosition[0, 1] < 340) || (CarsArrayPosition[0, 1] >= 1623 && CarsArrayPosition[0, 1] < 1785) || (CarsArrayPosition[0, 1] >= 750 && CarsArrayPosition[0, 1] < 974) || (CarsArrayPosition[0, 1] >= 2199 && CarsArrayPosition[0, 1] <= 2394) || (CarsArrayPosition[0, 1] >= 340 && CarsArrayPosition[0, 1] < 500))
                return 2;
            else if ((CarsArrayPosition[0, 1] > 1785 && CarsArrayPosition[0, 1] < 1975))
                return 2;
            else return 1;
        }
        int down()
        {
            if ((CarsArrayPosition[0, 1] >= 0 && CarsArrayPosition[0, 1] < 340) || (CarsArrayPosition[0, 1] >= 1623 && CarsArrayPosition[0, 1] < 1785) || (CarsArrayPosition[0, 1] >= 750 && CarsArrayPosition[0, 1] < 974) || (CarsArrayPosition[0, 1] >= 2199 && CarsArrayPosition[0, 1] <= 2394) || (CarsArrayPosition[0, 1] >= 340 && CarsArrayPosition[0, 1] < 500))
                return 2;
            else if ((CarsArrayPosition[0, 1] > 1785 && CarsArrayPosition[0, 1] < 1975))
                return 2;
            else return 3;
        }
        int Right()
        {
            if ((CarsArrayPosition[0, 1] >= 0 && CarsArrayPosition[0, 1] < 340) || (CarsArrayPosition[0, 1] >= 1623 && CarsArrayPosition[0, 1] < 1785) || (CarsArrayPosition[0, 1] >= 750 && CarsArrayPosition[0, 1] < 974) || (CarsArrayPosition[0, 1] >= 2199 && CarsArrayPosition[0, 1] <= 2394) || (CarsArrayPosition[0, 1] >= 340 && CarsArrayPosition[0, 1] < 500))
            
                    return 3;
            else if ((CarsArrayPosition[0, 1] > 1785 && CarsArrayPosition[0, 1] < 1975))
                return 3;
               
            else return 2;
        }
        int Left()
        {
            if ((CarsArrayPosition[0, 1] >= 0 && CarsArrayPosition[0, 1] < 340) || (CarsArrayPosition[0, 1] >= 1623 && CarsArrayPosition[0, 1] < 1785) || (CarsArrayPosition[0, 1] >= 750 && CarsArrayPosition[0, 1] < 974) || (CarsArrayPosition[0, 1] >= 2199 && CarsArrayPosition[0, 1] <= 2394) || (CarsArrayPosition[0, 1] >= 340 && CarsArrayPosition[0, 1] < 500))

                return 1;
            else if ((CarsArrayPosition[0, 1] > 1785 && CarsArrayPosition[0, 1] < 1975))
                return 1;

            else return 2;
        }




        ///  /// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// 
        void draw_page1()
        {
            spriteBatch.Begin();
            spriteBatch.Draw(lambur, GraphicsDevice.Viewport.Bounds, Color.White);
            spriteBatch.Draw(continueSign, new Vector2(GraphicsDevice.Viewport.Width * 1.5f / 5, GraphicsDevice.Viewport.Height * 5 / 6), null, Color.White, 0.0f, Vector2.Zero, 1.5f, SpriteEffects.None, 1.0f);
            spriteBatch.DrawString(font, "CARS CONTROLLER", new Vector2(10, 10), Color.YellowGreen);
            spriteBatch.DrawString(angleFont, "facebook.com/CarsController", new Vector2(502, 450), Color.White);

            spriteBatch.End();
            return;
        }
        /// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        void draw_page2()
        {
            spriteBatch.Begin();

            spriteBatch.Draw(hilux, GraphicsDevice.Viewport.Bounds, Color.White);
            if(!Guide.IsTrialMode)
            spriteBatch.Draw(levels, new Vector2(GraphicsDevice.Viewport.Width * 1.5f / 6, GraphicsDevice.Viewport.Height * 4.7f / 6), null, Color.White, 0.0f, Vector2.Zero, 1.5f, SpriteEffects.None, 1.0f);
            else spriteBatch.Draw(levels_lock, new Vector2(GraphicsDevice.Viewport.Width * 1.5f / 6, GraphicsDevice.Viewport.Height * 4.7f / 6), null, Color.White, 0.0f, Vector2.Zero, 1.5f, SpriteEffects.None, 1.0f);
            spriteBatch.DrawString(font, "please choose a level", new Vector2(120, 20), Color.Red);
            if (SOUND)
                spriteBatch.Draw(soundon, new Vector2(710, 0), null, Color.White, 0.0f, Vector2.Zero, 1f, SpriteEffects.None, 1.0f);
            else spriteBatch.Draw(soundoff, new Vector2(710, 0), null, Color.White, 0.0f, Vector2.Zero, 1f, SpriteEffects.None, 1.0f);
            spriteBatch.Draw(help, new Vector2(0, 0), null, Color.White, 0.0f, Vector2.Zero, 1f, SpriteEffects.None, 1.0f);
            spriteBatch.End();
            return;
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        void showRotAmpel(int i)
        {
            spriteBatch.Begin(); int j = 0, j2 = 0;
            j = (int)frame_counter / 300;
            j2 = (int)frame_counter2 / 300;
            if (j == 0 && i == 1)
                spriteBatch.Draw(red1, new Vector2(280 , 228), null, Color.White, -0.565f, Vector2.Zero, 1.10f, SpriteEffects.None, 1.0f);
            else if (j == 1 && i == 1)
                spriteBatch.Draw(red2, new Vector2(280 , 228), null, Color.White, -0.565f, Vector2.Zero, 1.10f, SpriteEffects.None, 1.0f);
            else if (j == 2 && i == 1)
                spriteBatch.Draw(red3, new Vector2(280 , 228 ), null, Color.White, -0.565f, Vector2.Zero, 1.10f, SpriteEffects.None, 1.0f);
            else if (j == 3 && i == 1)
                spriteBatch.Draw(red4, new Vector2(280 , 228 ), null, Color.White, -0.565f, Vector2.Zero, 1.10f, SpriteEffects.None, 1.0f);
            else if (j == 4 && i == 1)
                spriteBatch.Draw(red5, new Vector2(280 , 228 ), null, Color.White, -0.565f, Vector2.Zero, 1.10f, SpriteEffects.None, 1.0f);
            if (j2 == 0 && i == 2)
                spriteBatch.Draw(red_1, new Vector2(527 , 272 ), null, Color.White, -0.55f, Vector2.Zero, 1.1f, SpriteEffects.None, 1.0f);
            else if (j2 == 1 && i == 2)
                spriteBatch.Draw(red_2, new Vector2(527 , 272 ), null, Color.White, -0.55f, Vector2.Zero, 1.1f, SpriteEffects.None, 1.0f);
            else if (j2 == 2 && i == 2)
                spriteBatch.Draw(red_3, new Vector2(527 , 272 ), null, Color.White, -0.55f, Vector2.Zero, 1.1f, SpriteEffects.None, 1.0f);
            else if (j2 == 3 && i == 2)
                spriteBatch.Draw(red_4, new Vector2(527 , 272 ), null, Color.White, -0.55f, Vector2.Zero, 1.1f, SpriteEffects.None, 1.0f);
            else if (j2 == 4 && i == 2)
                spriteBatch.Draw(red_5, new Vector2(527 , 272 ), null, Color.White, -0.55f, Vector2.Zero, 1.1f, SpriteEffects.None, 1.0f);
            spriteBatch.End();
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////
        void showGrunAmpel(int i)
        {

            spriteBatch.Begin(); int j = 0, j2 = 0;
            j = (int)frame_counter / 120;
            j2 = (int)frame_counter2 / 120;
            if (j == 0 && i == 1)
                spriteBatch.Draw(green1, new Vector2(280 , 228 ), null, Color.White, -0.565f, Vector2.Zero, 1.10f, SpriteEffects.None, 1.0f);
            else if (j == 1 && i == 1)
                spriteBatch.Draw(green2, new Vector2(280 , 228), null, Color.White, -0.565f, Vector2.Zero, 1.10f, SpriteEffects.None, 1.0f);
            else if (j == 2 && i == 1)
                spriteBatch.Draw(green3, new Vector2(280, 228), null, Color.White, -0.565f, Vector2.Zero, 1.10f, SpriteEffects.None, 1.0f);
            else if (j == 3 && i == 1)
                spriteBatch.Draw(green4, new Vector2(280, 228 ), null, Color.White, -0.565f, Vector2.Zero, 1.10f, SpriteEffects.None, 1.0f);
            else if (j == 4 && i == 1)
                spriteBatch.Draw(green5, new Vector2(280 , 228 ), null, Color.White, -0.565f, Vector2.Zero, 1.10f, SpriteEffects.None, 1.0f);
            if (j2 == 0 && i == 2)
                spriteBatch.Draw(green_1, new Vector2(527 , 272 ), null, Color.White, -0.55f, Vector2.Zero, 1.1f, SpriteEffects.None, 1.0f);
            else if (j2 == 1 && i == 2)
                spriteBatch.Draw(green_2, new Vector2(527 , 272 ), null, Color.White, -0.55f, Vector2.Zero, 1.1f, SpriteEffects.None, 1.0f);
            else if (j2 == 2 && i == 2)
                spriteBatch.Draw(green_3, new Vector2(527 , 272), null, Color.White, -0.55f, Vector2.Zero, 1.1f, SpriteEffects.None, 1.0f);
            else if (j2 == 3 && i == 2)
                spriteBatch.Draw(green_4, new Vector2(527 , 272 ), null, Color.White, -0.55f, Vector2.Zero, 1.1f, SpriteEffects.None, 1.0f);
            else if (j2 == 4 && i == 2)
                spriteBatch.Draw(green_5, new Vector2(527 , 272), null, Color.White, -0.55f, Vector2.Zero, 1.1f, SpriteEffects.None, 1.0f);
            spriteBatch.End();
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        protected override void OnExiting(object sender, EventArgs args)
        {
            if (Guide.IsTrialMode)
                return;
            int acc = 0;

            using (IsolatedStorageFile file = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (BinaryWriter writer = new BinaryWriter(file.CreateFile("currentState")))
                {
                    if (!accident && page == 3 && !WIN )
                    {
                        writer.Write(Level);
                        writer.Write(RECORD);
                        writer.Write(SOUND);
                        writer.Write(CarsArrayPosition[0, 0]); writer.Write(CarsArrayPosition[0, 1]);
                        writer.Write(CarsArrayPosition[1, 0]); writer.Write(CarsArrayPosition[1, 1]);
                        writer.Write(CarsArrayPosition[2, 0]); writer.Write(CarsArrayPosition[2, 1]);
                        writer.Write(heart_counter);
                        writer.Write(modelPositionH.X);
                        writer.Write(modelPositionH.Y);
                        writer.Write(modelPositionH.Z);
                        writer.Write(pixelPositionH.X);
                        writer.Write(pixelPositionH.Y);
                        writer.Write(frame_counter);
                        writer.Write(frame_counter2);
                        writer.Write(ampel1);
                        writer.Write(ampel2);
                        writer.Write(ampleSound);
                        writer.Write(Divert[0]);
                         writer.Write(Divert[1]);
                         writer.Write(Divert[2]);
                        //writer.Write(PAUSE);
                        
                    }
                    else writer.Write(acc);
                }

            }
            base.OnExiting(sender, args);
        }

        protected override void OnActivated(object sender, EventArgs args)
        {
            // Load the data
            if (Guide.IsTrialMode)
                return;
            int acc = 0;
            using (IsolatedStorageFile file = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (BinaryReader reader = new BinaryReader(file.OpenFile("currentState",
                    FileMode.OpenOrCreate)))
                {
                    if (reader.BaseStream.Length > 0)
                    {
                        acc = reader.Read();
                    }
                    if (acc > 0)
                    {
                        cleanFile = true;
                        reader.BaseStream.Position = 4;
                        Unfinished = true;
                        Level = acc;
                        RECORD = reader.ReadInt64();
                        reader.BaseStream.Position = 12;
                        SOUND = reader.ReadBoolean();
                        
                        reader.BaseStream.Position = 13;
                        CarsArrayPosition[0, 0] = reader.ReadInt32();
                        reader.BaseStream.Position = 17;
                        CarsArrayPosition[0, 1] = reader.ReadInt32();
                        reader.BaseStream.Position = 21;
                        CarsArrayPosition[1, 0] = reader.ReadInt32();
                        reader.BaseStream.Position = 25;
                        CarsArrayPosition[1, 1] = reader.ReadInt32();
                        reader.BaseStream.Position = 29;
                        CarsArrayPosition[2, 0] = reader.ReadInt32();
                        reader.BaseStream.Position = 33;
                        CarsArrayPosition[2, 1] = reader.ReadInt32();
                        reader.BaseStream.Position = 37;
                        heart_counter = reader.ReadInt32();
                        reader.BaseStream.Position =41 ;
                        modelPositionH.X = reader.ReadSingle() ;
                        reader.BaseStream.Position = 45;
                        modelPositionH.Y = reader.ReadSingle();
                        reader.BaseStream.Position = 49;
                        modelPositionH.Z = reader.ReadSingle();
                        reader.BaseStream.Position = 53;
                        pixelPositionH.X = reader.ReadSingle();
                        reader.BaseStream.Position = 57;
                        pixelPositionH.Y = reader.ReadSingle();
                        reader.BaseStream.Position = 61;
                        frame_counter= reader.ReadInt32();
                        reader.BaseStream.Position = 65;
                        frame_counter2 = reader.ReadInt32();
                        reader.BaseStream.Position = 69;
                        ampel1 = reader.ReadBoolean();
                        reader.BaseStream.Position = 70;
                        ampel2 = reader.ReadBoolean();
                        reader.BaseStream.Position = 71;
                        ampleSound = reader.ReadBoolean();
                        reader.BaseStream.Position = 72;
                        Divert[0]= reader.ReadBoolean();
                        reader.BaseStream.Position = 73;
                        Divert[1]= reader.ReadBoolean();
                        reader.BaseStream.Position = 74;
                        Divert[2]= reader.ReadBoolean();
                        //PAUSE = reader.ReadBoolean();
                        if (!MediaPlayer.GameHasControl)
                            SOUND = false;

                        
                    }
                }
                
            }

            base.OnActivated(sender, args);
        }
        /// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// 
        void draw_page4()
        {
            spriteBatch.Begin();
            spriteBatch.Draw(newgame, new Vector2(GraphicsDevice.Viewport.Width * 1.5f / 5, (GraphicsDevice.Viewport.Height * 5 / 6) - 55), null, Color.White, 0.0f, Vector2.Zero, 1.5f, SpriteEffects.None, 1.0f);
            spriteBatch.Draw(continueSign, new Vector2(GraphicsDevice.Viewport.Width * 1.5f / 5, GraphicsDevice.Viewport.Height * 5 / 6), null, Color.White, 0.0f, Vector2.Zero, 1.5f, SpriteEffects.None, 1.0f);
            spriteBatch.DrawString(font, "do you want to continue", new Vector2(70, 20), Color.Red);
            if(!SW_stopBG)
            spriteBatch.DrawString(font, "last game?", new Vector2(225, 70), Color.Red);
            spriteBatch.DrawString(angleFont, "facebook.com/CarsControl", new Vector2(515, 450), Color.White);
            if (SOUND)
                spriteBatch.Draw(soundon, new Vector2(710, 0), null, Color.White, 0.0f, Vector2.Zero, 1f, SpriteEffects.None, 1.0f);
            else spriteBatch.Draw(soundoff, new Vector2(710, 0), null, Color.White, 0.0f, Vector2.Zero, 1f, SpriteEffects.None, 1.0f);
            spriteBatch.End();
            return;
        }
        /// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// 
        void draw_page5()
        {
            spriteBatch.Begin();
            //spriteBatch.Draw(newgame, new Vector2(GraphicsDevice.Viewport.Width * 1.5f / 5, (GraphicsDevice.Viewport.Height * 5 / 6) - 55), null, Color.White, 0.0f, Vector2.Zero, 1.5f, SpriteEffects.None, 1.0f);
            spriteBatch.Draw(ok, new Vector2(GraphicsDevice.Viewport.Width * 1.5f / 5, GraphicsDevice.Viewport.Height * 5 / 6), null, Color.White, 0.0f, Vector2.Zero, 1.5f, SpriteEffects.None, 1.0f);
            //spriteBatch.DrawString(font, "do you want to continue", new Vector2(70, 20), Color.Red);
            //spriteBatch.DrawString(font, "last game?", new Vector2(225, 70), Color.Red);
            spriteBatch.DrawString(angleFont2, "There  are   two cars in easy level and  medium  ", new Vector2(0, 10), Color.White);
            spriteBatch.DrawString(angleFont2, "level, and  there are three  cars in  difficult  ", new Vector2(0, 40), Color.White);
            spriteBatch.DrawString(angleFont2, "level. Another difference between the levels is  ", new Vector2(0, 70), Color.White);
            spriteBatch.DrawString(angleFont2, "the cars speed. You have to  control the yellow   ", new Vector2(0, 100), Color.White);
            spriteBatch.DrawString(angleFont2, "car and  take  red  Hearts  by  it.  Other cars   ", new Vector2(0, 130), Color.White);
            spriteBatch.DrawString(angleFont2, "are under the computer's control.Nevertheless, ", new Vector2(0, 160), Color.White);
            spriteBatch.DrawString(angleFont2, "you can control them to avoid any accidents.", new Vector2(0, 190), Color.White);
            spriteBatch.DrawString(angleFont2, "If you take the red Heart you will get 100 points  ", new Vector2(0, 220), Color.White);
            spriteBatch.DrawString(angleFont2, "and If you take A,B,C sequentially you will get ", new Vector2(0, 250), Color.White);
            spriteBatch.DrawString(angleFont2, "500 points. ", new Vector2(0, 280), Color.White);
            spriteBatch.DrawString(angleFont2, "The maximum score for each level is 200,000. ", new Vector2(0, 310), Color.White);
            
            spriteBatch.End();
            return;
        }


        /// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
       
        void draw_steps()
        {
            spriteBatch.Begin();
            if(STEPS[0,0]>0)
            spriteBatch.DrawString(font, "A", new Vector2(STEPS[0,0], STEPS[1,0]), Color.Black);
            if (STEPS[0, 1] > 0)
            spriteBatch.DrawString(font, "B", new Vector2(STEPS[0,1], STEPS[1,1]), Color.Black);
            if (STEPS[0, 2] > 0)
            spriteBatch.DrawString(font, "C", new Vector2(STEPS[0,2], STEPS[1,2]), Color.Black);
            spriteBatch.End();
            return;
        }
        /// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        protected override void Draw(GameTime gameTime)
        {

            GraphicsDevice.Clear(Color.Black);

            if (page == 1)
            {
                if (cleanFile)
                {
                    using (IsolatedStorageFile file = IsolatedStorageFile.GetUserStoreForApplication())
                    {
                        using (BinaryWriter writer = new BinaryWriter(file.CreateFile("currentState")))
                        {
                            writer.Write(0);
                        }

                    }
                    cleanFile = false;
                }
                if (Unfinished)
                { draw_page4(); }
                else draw_page1();
            }
            else if (page == 2)
                draw_page2();
            else if (page == 5)
                draw_page5();

            else if (page == 3)
            {
                if (sw_firstStart && SOUND && !finish && !PAUSE && !ampleSound)
                {
                    MediaPlayer.Play(carsound);
                    MediaPlayer.IsRepeating = true;
                    sw_firstStart = false;
                }


                if (!accident && !WIN )
                {
                    if (ampel1)
                        showGrunAmpel(1);
                    else
                        showRotAmpel(1);
                    if (ampel2)
                        showGrunAmpel(2);
                    else
                        showRotAmpel(2);
                }
                spriteBatch.Begin();
                if(Level==1)
                spriteBatch.Draw(fall, GraphicsDevice.Viewport.Bounds, Color.White);
                else if (Level == 2)
                    spriteBatch.Draw(summer, GraphicsDevice.Viewport.Bounds, Color.White);
                else if (Level == 3)
                    spriteBatch.Draw(winter, GraphicsDevice.Viewport.Bounds, Color.White);

                if (Level < 3)
                {
                   
                        spriteBatch.Draw(circle, new Vector2(670, 10), null, Color.White, 0.0f, Vector2.Zero, .90f, SpriteEffects.None, 1.0f);
                    
                    if (CarsRoute[1] == 1)
                        spriteBatch.Draw(wanet2_left, new Vector2(0, 0), null, Color.White, 0.0f, Vector2.Zero, .9f, SpriteEffects.None, 1.0f);
                    else if (CarsRoute[1] == 2)
                        spriteBatch.Draw(wanet2_grade, new Vector2(0, 0), null, Color.White, 0.0f, Vector2.Zero, .9f, SpriteEffects.None, 1.0f);
                    else
                        spriteBatch.Draw(wanet2_right, new Vector2(0, 0), null, Color.White, 0.0f, Vector2.Zero, .9f, SpriteEffects.None, 1.0f);



                }

                else if (Level == 3 || Level == 4)
                {
                    
                        spriteBatch.Draw(circle, new Vector2(670, 10), null, Color.White, 0.0f, Vector2.Zero, .90f, SpriteEffects.None, 1.0f);
                    
                    if (CarsRoute[1] == 1)
                        spriteBatch.Draw(wanet2_left, new Vector2(0, 0), null, Color.White, 0.0f, Vector2.Zero, .9f, SpriteEffects.None, 1.0f);
                    else if (CarsRoute[1] == 2)
                        spriteBatch.Draw(wanet2_grade, new Vector2(0, 0), null, Color.White, 0.0f, Vector2.Zero, .9f, SpriteEffects.None, 1.0f);
                    else
                        spriteBatch.Draw(wanet2_right, new Vector2(0, 0), null, Color.White, 0.0f, Vector2.Zero, .9f, SpriteEffects.None, 1.0f);

                    if (CarsRoute[2] == 1)
                        spriteBatch.Draw(wanet3_left, new Vector2(0, 57), null, Color.White, 0.0f, Vector2.Zero, .9f, SpriteEffects.None, 1.0f);
                    else if (CarsRoute[2] == 2)
                        spriteBatch.Draw(wanet3_grade, new Vector2(0, 57), null, Color.White, 0.0f, Vector2.Zero, .9f, SpriteEffects.None, 1.0f);
                    else
                        spriteBatch.Draw(wanet3_right, new Vector2(0, 57), null, Color.White, 0.0f, Vector2.Zero, .9f, SpriteEffects.None, 1.0f);

                }
                if (!accident && !WIN )
                {
                    if (SW[0] == true && CarsRoute[0] == 2)
                    {
                        spriteBatch.Draw(grade, new Vector2(pos0.X, pos0.Y), null, Color.White, -1.57f - modelRotation0, Vector2.Zero, .8f, SpriteEffects.None, 1.0f);
                    }
                    else if (SW[0] == true && CarsRoute[0] == 1)
                    {
                        if (leftORright[0])
                            spriteBatch.Draw(left, new Vector2(pos0.X, pos0.Y), null, Color.White, -1.57f - modelRotation0, Vector2.Zero, .8f, SpriteEffects.None, 1.0f);
                        else spriteBatch.Draw(right, new Vector2(pos0.X, pos0.Y), null, Color.White, -1.57f - modelRotation0, Vector2.Zero, .8f, SpriteEffects.None, 1.0f);
                    }
                    else if (SW[0] == true && CarsRoute[0] == 3)
                    {
                        if (leftORright[0])
                            spriteBatch.Draw(right, new Vector2(pos0.X, pos0.Y), null, Color.White, -1.57f - modelRotation0, Vector2.Zero, .8f, SpriteEffects.None, 1.0f);
                        else spriteBatch.Draw(left, new Vector2(pos0.X, pos0.Y), null, Color.White, -1.57f - modelRotation0, Vector2.Zero, .8f, SpriteEffects.None, 1.0f);
                    }
                    if (SW[1] == true && CarsRoute[1] == 2)
                    {
                        spriteBatch.Draw(grade, new Vector2(pos1.X, pos1.Y), null, Color.White, -1.57f - modelRotation1, Vector2.Zero, .8f, SpriteEffects.None, 1.0f);
                    }
                    else if (SW[1] == true && CarsRoute[1] == 1)
                    {
                        if (leftORright[1])
                            spriteBatch.Draw(left, new Vector2(pos1.X, pos1.Y), null, Color.White, -1.57f - modelRotation1, Vector2.Zero, .8f, SpriteEffects.None, 1.0f);
                        else spriteBatch.Draw(right, new Vector2(pos1.X, pos1.Y), null, Color.White, -1.57f - modelRotation1, Vector2.Zero, .8f, SpriteEffects.None, 1.0f);
                    }
                    else if (SW[1] == true && CarsRoute[1] == 3)
                    {
                        if (leftORright[1])
                            spriteBatch.Draw(right, new Vector2(pos1.X, pos1.Y), null, Color.White, -1.57f - modelRotation1, Vector2.Zero, .8f, SpriteEffects.None, 1.0f);
                        else spriteBatch.Draw(left, new Vector2(pos1.X, pos1.Y), null, Color.White, -1.57f - modelRotation1, Vector2.Zero, .8f, SpriteEffects.None, 1.0f);
                    }
                    if (SW[2] == true && CarsRoute[2] == 2 && (Level == 3 || Level == 4))
                    {
                        spriteBatch.Draw(grade, new Vector2(pos2.X, pos2.Y), null, Color.White, -1.57f - modelRotation2, Vector2.Zero, .8f, SpriteEffects.None, 1.0f);
                    }
                    else if (SW[2] == true && CarsRoute[2] == 1 && (Level == 3 || Level == 4))
                    {
                        if (leftORright[2])
                            spriteBatch.Draw(left, new Vector2(pos2.X, pos2.Y), null, Color.White, -1.57f - modelRotation2, Vector2.Zero, .8f, SpriteEffects.None, 1.0f);
                        else spriteBatch.Draw(right, new Vector2(pos2.X, pos2.Y), null, Color.White, -1.57f - modelRotation2, Vector2.Zero, .8f, SpriteEffects.None, 1.0f);
                    }
                    else if (SW[2] == true && CarsRoute[2] == 3 && (Level == 3 || Level == 4))
                    {
                        if (leftORright[2])
                            spriteBatch.Draw(right, new Vector2(pos2.X, pos2.Y), null, Color.White, -1.57f - modelRotation2, Vector2.Zero, .8f, SpriteEffects.None, 1.0f);
                        else spriteBatch.Draw(left, new Vector2(pos2.X, pos2.Y), null, Color.White, -1.57f - modelRotation2, Vector2.Zero, .8f, SpriteEffects.None, 1.0f);
                    }
                }
                spriteBatch.End();
                if (!accident && !WIN )
                {
                    if (ampel1)
                        showGrunAmpel(1);

                    else
                        showRotAmpel(1);

                    if (ampel2)
                        showGrunAmpel(2);

                    else
                        showRotAmpel(2);

                }
                spriteBatch.Begin();
                if (accident)
                    spriteBatch.Draw(gameOver, new Vector2(GraphicsDevice.Viewport.Width * 1.6f / 5, GraphicsDevice.Viewport.Height * 1.5f / 5), null, Color.White, 0.0f, Vector2.Zero, 2.0f, SpriteEffects.None, 1.0f);
                if (WIN)
                {
                    if (!Guide.IsTrialMode)
                        spriteBatch.Draw(winner, new Vector2(GraphicsDevice.Viewport.Width * 1.6f / 5, GraphicsDevice.Viewport.Height * 1.5f / 5), null, Color.White, 0.0f, Vector2.Zero, 2.0f, SpriteEffects.None, 1.0f);
                    else spriteBatch.Draw(fullversion, new Vector2(GraphicsDevice.Viewport.Width * 1.6f / 5, GraphicsDevice.Viewport.Height * 1.5f / 5), null, Color.White, 0.0f, Vector2.Zero, 2.0f, SpriteEffects.None, 1.0f);
                }
                
                spriteBatch.Draw(SCORE, new Vector2(280, 0), null, Color.White, 0.0f, Vector2.Zero, 1.25f, SpriteEffects.None, 1.0f);
                spriteBatch.Draw(jump, new Vector2(2, 445), null, Color.White, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 1.0f);
                spriteBatch.DrawString(angleFont, "score ", new Vector2(300, 0), Color.Black);
                spriteBatch.DrawString(angleFont, String.Format("{0,5}", RECORD), new Vector2(460, 0), Color.Black);
                spriteBatch.DrawString(angleFont, "record ", new Vector2(300, 30), Color.Black);
                spriteBatch.DrawString(angleFont, String.Format("{0,5}", MAX), new Vector2(460, 30), Color.Black);
                if (PAUSE)
                {
                    spriteBatch.Draw(PlayPic, new Vector2(760, 440), null, Color.White, 0.0f, Vector2.Zero, .4f, SpriteEffects.None, 1.0f);
                    if (SOUND)
                    MediaPlayer.Pause();
                }
                else spriteBatch.Draw(PausePic, new Vector2(760, 440), null, Color.White, 0.0f, Vector2.Zero, .085f, SpriteEffects.None, 1.0f);
                spriteBatch.End();

                if (!accident && !WIN )
                {
                    float radius = GetMaxMeshRadius(model0);
                    draw_steps();

                    Matrix[] transforms0 = new Matrix[model0.Bones.Count];
                    model0.CopyAbsoluteBoneTransformsTo(transforms0);
                    foreach (ModelMesh mesh in model0.Meshes)
                    {
                        if (modelPosition0.X / 10000 != 0)
                        {
                            word1 = transforms0[mesh.ParentBone.Index] * Matrix.CreateRotationY(modelRotation0) * Matrix.CreateTranslation(modelPosition0) * Matrix.CreateScale(0.0006f);
                            foreach (BasicEffect effect in mesh.Effects)
                            {
                                effect.World = transforms0[mesh.ParentBone.Index] * Matrix.CreateRotationY(modelRotation0) * Matrix.CreateTranslation(modelPosition0) * Matrix.CreateScale(0.0006f);
                                effect.View = Matrix.CreateLookAt(cam_position, new Vector3(0, 0, 0f), Vector3.Up);
                                effect.Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, GraphicsDevice.Viewport.AspectRatio, 1.0f, 100.0f);
                            }

                            mesh.Draw();
                        }
                    }
                    Matrix[] transforms1 = new Matrix[model1.Bones.Count];
                    model1.CopyAbsoluteBoneTransformsTo(transforms1);
                    foreach (ModelMesh mesh in model1.Meshes)
                    {
                        if (modelPosition1.X / 10000 != 0)
                        {
                            foreach (BasicEffect effect in mesh.Effects)
                            {
                                effect.World = transforms1[mesh.ParentBone.Index] * Matrix.CreateRotationY(modelRotation1) * Matrix.CreateTranslation(modelPosition1) * Matrix.CreateScale(0.0006f);
                                effect.View = Matrix.CreateLookAt(cam_position, new Vector3(0, 0, 0f), Vector3.Up);
                                effect.Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, GraphicsDevice.Viewport.AspectRatio, 1.0f, 100.0f);
                            }
                            mesh.Draw();
                        }

                    }
                    if (Level == 3 || Level == 4)
                    {
                        Matrix[] transforms2 = new Matrix[model2.Bones.Count];
                        model2.CopyAbsoluteBoneTransformsTo(transforms2);
                        foreach (ModelMesh mesh in model2.Meshes)
                        {
                            if (modelPosition2.X != 10000)
                            {
                                foreach (BasicEffect effect in mesh.Effects)
                                {
                                    effect.World = transforms2[mesh.ParentBone.Index] * Matrix.CreateRotationY(modelRotation2) * Matrix.CreateTranslation(modelPosition2) * Matrix.CreateScale(0.0006f);
                                    effect.View = Matrix.CreateLookAt(cam_position, new Vector3(0, 0, 0f), Vector3.Up);
                                    effect.Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, GraphicsDevice.Viewport.AspectRatio, 1.0f, 100.0f);
                                }
                                mesh.Draw();
                            }

                        }
                    }
                    if (!heartAccident && ((Level == 1 && heart_counter < 650) || (Level > 1 && heart_counter < 450)))
                    {
                        Matrix[] transformsH = new Matrix[modelheart.Bones.Count];
                        modelheart.CopyAbsoluteBoneTransformsTo(transformsH);
                        foreach (ModelMesh mesh in modelheart.Meshes)
                        {
                            if (modelPositionH.X != 10000)
                            {
                                foreach (BasicEffect effect in mesh.Effects)
                                {
                                    effect.World = transformsH[mesh.ParentBone.Index] * Matrix.CreateRotationY(modelRotationH) * Matrix.CreateTranslation(modelPositionH) * Matrix.CreateScale(.10f);
                                    effect.View = viewH;
                                    effect.Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, GraphicsDevice.Viewport.AspectRatio, 1.0f, 100.0f);
                                }
                                mesh.Draw();
                            }

                        }
                    }

                    if (backORexit)
                    {
                        spriteBatch.Begin();
                        spriteBatch.Draw(back, new Vector2(GraphicsDevice.Viewport.Width * 1.85f / 5, GraphicsDevice.Viewport.Height * 1.5f / 5), null, Color.White, 0.0f, Vector2.Zero, 2.0f, SpriteEffects.None, 1.0f);
                        spriteBatch.End();
                          
                    }
                    
                }
            }
            if ((SW_stopBG && page == 2) || (SW_stopBG && page == 1 && Unfinished))
            {
                spriteBatch.Begin();
                spriteBatch.Draw(stopmusic, new Vector2(GraphicsDevice.Viewport.Width * 1.35f / 5, GraphicsDevice.Viewport.Height * 1.0f / 5), null, Color.White, 0.0f, Vector2.Zero, 2.0f, SpriteEffects.None, 1.0f);
                spriteBatch.End();

            }
        }

        ////////////////////////////////////////////////////
        private Matrix GetParentTransform(Model m, ModelBone mb)
        {
            return (mb == m.Root) ? mb.Transform :
                mb.Transform * GetParentTransform(m, mb.Parent);
        }
        private float GetMaxMeshRadius(Model m)
        {
            float radius = 0.0f;
            foreach (ModelMesh mm in m.Meshes)
            {
                if (mm.BoundingSphere.Radius > radius)
                {
                    radius = mm.BoundingSphere.Radius;
                }
            }
            return radius;
        }

    }
}
